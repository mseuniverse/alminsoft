unit dubbleform;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,mseforms;
type
 tdubblefo = class(tmseform)
 end;
var
 dubblefo: tdubblefo;
implementation
uses
 dubbleform_mfm;
end.
