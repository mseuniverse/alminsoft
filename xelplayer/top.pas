unit top;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,mseforms;
type
 ttopfo = class(tmseform)
 end;
var
 topfo: ttopfo;
implementation
uses
 top_mfm;
end.
