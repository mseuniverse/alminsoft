program xelplayer;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
{$ifdef FPC}
 {$ifdef mswindows}{$apptype gui}{$endif}
{$endif}
uses
 {$ifdef FPC}{$ifdef linux}cthreads,{$endif}{$endif}
 msegui,mseforms,main, loadform, inifiles,
 {$ifdef linux} process, classes,{$endif} sysutils,
 msesysintf;

var {$ifdef linux}
    p : tprocess;
    sl : tstringlist;
    {$endif}
    i,c : integer;
    s : string;
    f : textfile;
    fi: tinifile;
    b : boolean;
    


function dirtowin(str : string) : string;
var i : integer;
begin
  IF LENGTH(STR) > 0 then
  {$ifdef linux} 
  if str[length(str)] <> '/' then str := str + '/';
  {$endif} 
  {$ifdef windows} 
  IF str[1] = '/' THEN DELETE(str,1,1);
  IF LENGTH(STR) > 0 THEN 
  for i := 1 to length(str) do 
    if str[i] = '/' then str[i] := '\';  
  if length(str) > 0 then
  if str[length(str)] <> '\' then str := str + '\';
  {$endif} 
  result := str;
end;
   
begin
 S := sys_getuserhomedir;
 {$ifdef windows}IF S > '' THEN IF S[1] = '/' THEN DELETE(S,1,1);{$endif}
 forcedirectories(S + '/.Almin-Soft/xelplayer');
 
 //write playlist
 if ParamCount > 0 then 
    begin
      assignfile(f,S + '/.Almin-Soft/xelplayer/xelplayer.pl');
      rewrite(f); 
      for i := 1 to ParamCount do writeln(f,ParamStr(i));
      closefile(f);
    end;
        
  fi := tinifile.create(dirtowin(extractfilepath(sys_getapplicationpath)) + 'xelplayer15.conf');
  b := fi.readbool('xelplayer','useonecopy', true);
  fi.free;
 if b then
 BEGIN 
 {$ifdef linux}
 sl := tstringlist.create;
 p := tprocess.create(nil);

 p.commandline := 'ps -C xelplayer'; 
 p.Options := p.Options +[poWaitOnExit,poUsePipes];
 p.execute;
 sl.LoadFromStream(p.Output);
 c := 0;
 if sl.count > 0 then
 for i :=0 to sl.count - 1 do 
   if system.pos('xelplayer',sl[i]) > 0 then inc(c);
 if c > 1 then   
   begin
     exit;
   end;
 p.free;
 sl.free;
 {$endif}
 {$ifdef windows}
 if fileexists(S + '/.Almin-Soft/xelplayer/xelplayer.lock') then exit;
 assignfile(f,S + '/.Almin-Soft/xelplayer/xelplayer.lock');
 {$I-}
 rewrite(f);
 closefile(f);
 {$I+} 
 {$endif}
 END;
  loadfo := tloadfo.create(application);
  loadfo.show;
  loadfo.update; 
 application.createform(tmainfo,mainfo);
 application.run;
 {$ifdef windows}
 if fileexists(S + '/.Almin-Soft/xelplayer/xelplayer.lock')
    then deletefile(S + '/.Almin-Soft/xelplayer/xelplayer.lock');
 {$endif} 
end.