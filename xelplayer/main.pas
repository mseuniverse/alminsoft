{   alsmplayer Copyright (c) 2011-2012 by Alexandre Minoshi

    version see in tmplayer.create procedure

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

    minoshi@yandex.ru

    Analog TV support by Coyot.RusH
}
unit main;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,mseforms,msesplitter,
 msesimplewidgets,msewidgets,msetimer,msedataedits,mseedit,msegrids,mseifiglob,
 msestrings,msetypes,msewidgetgrid,msescrollbar,msestatfile,msestream,msetabs,
 msegraphedits,sysutils,msebitmap,msedatanodes,msefiledialog,mselistbrowser,
 msesys,msedataimage,classes,alsmplayer,msewindowwidget,mseimage,msetraywidget,
 msepolygon,{} inifiles,msefilechange  ,msedock,
 msesysdnd,msedrag,msedragglob,msemime,
 msearrayutils, mp3file;

const 
 knownformats: array[0..1] of msestring = ('text/uri-list','CF_HDROP');
 
type
 dropfilesty = record
  pfiles: dword;
  pt: pointty;    
  fnc: longbool;    
  fwide: longbool;  
 end;           
 pdropfilesty = ^dropfilesty;
 
type
 tmainfo = class(tdockform)
   s_top: tspacer;
   s_bottom: tspacer;
   s_middle: tspacer;
   b_windowstyle: trichbutton;
   b_fullscreen: trichbutton;
   s_main: tspacer;
   s_playlist: tspacer;
   spl1: tsplitter;
   timer_resize: ttimer;
   timer_move: ttimer;
   trichbutton1: trichbutton;
   b_minimize: trichbutton;
   sl_position: tslider;
   b_volume: trichbutton;
   pb_volume: tprogressbar;
   sl_volume: tslider;
   tw_playlist: ttabwidget;
   tp_pl: ttabpage;
   ttabpage12: ttabpage;
   wg_playlist: twidgetgrid;
   se_name: tstringedit;
   se_url: tstringedit;
   se_emblem: tstringedit;
   S_TUNER: tspacer;
   st: tstringgrid;
   tbutton2: tbutton;
   tbutton6: tbutton;
   tbutton5: tbutton;
   e_tvname: tstringedit;
   e_normtv: tdropdownlistedit;
   e_freqtv: tintegeredit;
   ttabpage11: ttabpage;
   b_dubble: trichbutton;
   fr_panels: tframecomp;
   tspacer1: tspacer;
   tw_main: ttabwidget;
   ttabpage3: ttabpage;
   ttabpage7: ttabpage;
   dd_dir: tdirdropdownedit;
   flv: tfilelistview;
   ttabpage13: ttabpage;
   ttabpage4: ttabpage;
   tp_radio: ttabpage;
   tp_tv: ttabpage;
   TP_WEBCAM: ttabpage;
   tp_dvd: ttabpage;
   tp_TUNER: ttabpage;
   spl2: tsplitter;
   tspacer3: tspacer;
   tw_addons: ttabwidget;
   ttabpage14: ttabpage;
   tscrollbox2: tscrollbox;
   l_trackinfo: tlabel;
   trichbutton25: trichbutton;
   frame_edits: tframecomp;
   frame_buttons: tframecomp;
   wg_last: twidgetgrid;
   se_name_last: tstringedit;
   se_url_last: tstringedit;
   se_emblem_last: tstringedit;
   b_maximize: trichbutton;
   trichbutton11: trichbutton;
   trichbutton26: trichbutton;
   trichbutton12: trichbutton;
   wg_myplaylists: twidgetgrid;
   se_myplaylist: tstringedit;
   b_reloadplaylists: trichbutton;
   trichbutton13: trichbutton;
   trichbutton16: trichbutton;
   b_add_myplaylist_To_Playlist: trichbutton;
   b_open_from_myplaylists: trichbutton;
   trichbutton18: trichbutton;
   trichbutton19: trichbutton;
   trichbutton20: trichbutton;
   trichbutton21: trichbutton;
   wg_radio: twidgetgrid;
   wg_tv: twidgetgrid;
   se_name_tv: tstringedit;
   se_name_radio: tstringedit;
   di_emblem_tv: tdataimage;
   di_emblem_radio: tdataimage;
   se_site_tv: tstringedit;
   se_url_tv: tstringedit;
   se_emblem_tv: tstringedit;
   se_url_radio: tstringedit;
   se_site_radio: tstringedit;
   se_emblem_radio: tstringedit;
   trichbutton22: trichbutton;
   trichbutton24: trichbutton;
   b_edit_tv: trichbutton;
   trichbutton28: trichbutton;
   trichbutton29: trichbutton;
   b_edit_radio: trichbutton;
   b_add_from_radiolist: trichbutton;
   b_open_from_radiolist: trichbutton;
   b_add_from_tvlist: trichbutton;
   b_open_from_tvlist: trichbutton;
   s_bottom_pos: tspacer;
   s_bottom_right: tspacer;
   b_menu: trichbutton;
   pm_pl: tpopupmenu;
   FD: tfiledialog;
   s_btn_center: tspacer;
   s_btn_player: tspacer;
   b_next: trichbutton;
   b_stop: trichbutton;
   b_play: trichbutton;
   b_prev: trichbutton;
   s_btn_dvd: tspacer;
   trichbutton37: trichbutton;
   trichbutton38: trichbutton;
   trichbutton39: trichbutton;
   trichbutton40: trichbutton;
   trichbutton41: trichbutton;
   trichbutton44: trichbutton;
   trichbutton45: trichbutton;
   b_playDVD: trichbutton;
   trichbutton8: trichbutton;
   trichbutton3: trichbutton;
   edit_: tedit;
   pm_flv: tpopupmenu;
   pm_last: tpopupmenu;
   pm_mypls: tpopupmenu;
   pm_chanels: tpopupmenu;
   pm_main: tpopupmenu;
   trichbutton33: trichbutton;
   
   player: tmplayer;
   player2: tmplayer;
   se_cur: tstringedit;
   l_pos: tlabel;
   s_video: tspacer;
   ww_video: twindowwidget;
   b_animate: trichbutton;
   im_emblem: timage;
   s_left: tspacer;
   sb_left: tscrollbox;
   s_l_1: tspacer;
   bl3: trichbutton;
   bl2: trichbutton;
   bl1: trichbutton;
   bl0: trichbutton;
   s_l_2: tspacer;
   bl4: trichbutton;
   bl5: trichbutton;
   b_l_2: trichbutton;
   b_l_1: trichbutton;
   tspacer2: tspacer;
   trichbutton9: trichbutton;
   b_settings: trichbutton;
   ttabpage1: ttabpage;
   tw_sets: ttabwidget;
   ttabpage16: ttabpage;
   ttabpage17: ttabpage;
   ttabpage8: ttabpage;
   im_animate: timagelist;
   main_timer: ttimer;
   tspacer4: tspacer;
   b_playlist: trichbutton;
   b_library: trichbutton;
   trichbutton34: trichbutton;
   tdockhandle1: tdockhandle;
   ddd_dvdpath: tdirdropdownedit;
   ttabpage2: ttabpage;
   ddd_webcam: tdropdownlistedit;
   b_update_devices: trichbutton;
   l_tvtuner_capt: tlabel;
   l_dvd_capt: tlabel;
   l_webcam_capt: tlabel;
   f_top: tfacecomp;
   fr_form: tframecomp;
   f_scrollH: tfacecomp;
   f_scrollV: tfacecomp;
   f_buttons: tfacecomp;
   f_topbuttons: tfacecomp;
   fr_mainbuttons: tframecomp;
   f_mainbuttons: tfacecomp;
   fr_progress: tframecomp;
   pb_position: tprogressbar;
   player_preview: tmplayer;
   im_buttons: timagelist;
   s_bottom_top: tspacer;
   l_video_inf: tlabel;
   tspacer6: tspacer;
   tspacer7: tspacer;
   b_add_from_flv: trichbutton;
   b_open_from_flv: trichbutton;
   trichbutton15: trichbutton;
   trichbutton14: trichbutton;
   b_reread_flv: trichbutton;
   trichbutton23: trichbutton;
   b_home: trichbutton;
   l_video_inf2: tlabel;
   f_bottom: tfacecomp;
   fr_topbuttoms: tframecomp;
   timer_animate: ttimer;
   f_tabbutton: tfacecomp;
   b_close2: trichbutton;
   fr_bottombutton: tframecomp;
   f_bottombutton: tfacecomp;
   ddd_audioCDdevice: tdropdownlistedit;
   b_set_dvd_device: tbooleaneditradio;
   trichbutton5: trichbutton;
   tbooleaneditradio2: tbooleaneditradio;
   s_pl_top: tspacer;
   b_forward: trichbutton;
   trichbutton30: trichbutton;
   trichbutton6: trichbutton;
   b_clear: trichbutton;
   im_fileicons: timagelist;
   b_menuflv: trichbutton;
   filechangenotifyer: tfilechangenotifyer;
   wg_fl: twidgetgrid;
   se_fname: tstringedit;
   di_fl: tdataicon;
   se_furl: tstringedit;
   tlabel3: tlabel;
   player_info: tmplayer;
   fr_menu: tframecomp;
   b_add_from_lastplayed: trichbutton;
   b_l_3: trichbutton;
   s_l_3: tspacer;
   bl8: trichbutton;
   bl7: trichbutton;
   bl6: trichbutton;
   twindowwidget1: twindowwidget;
   tscrollbox1: tscrollbox;
   ber3: tbooleaneditradio;
   ber2: tbooleaneditradio;
   ber1: tbooleaneditradio;
   tlabel1: tlabel;
   trichbutton4: trichbutton;
   pb_gamma: tprogressbar;
   pb_hue: tprogressbar;
   pb_contrast: tprogressbar;
   pb_saturation: tprogressbar;
   pb_brightness: tprogressbar;
   sl_brightness: tslider;
   sl_saturation: tslider;
   sl_contrast: tslider;
   sl_HUE: tslider;
   sl_gamma: tslider;
   tscrollbox3: tscrollbox;
   pb_balance: tprogressbar;
   trichbutton71: trichbutton;
   tspacer5: tspacer;
   ber_audio0: tbooleaneditradio;
   ber_audio1: tbooleaneditradio;
   sl_balance: tslider;
   trichbutton35: trichbutton;
   sl_eq10: tslider;
   sl_eq9: tslider;
   sl_eq8: tslider;
   sl_eq7: tslider;
   sl_eq6: tslider;
   sl_eq5: tslider;
   sl_eq4: tslider;
   sl_eq3: tslider;
   sl_eq2: tslider;
   sl_eq1: tslider;
   tscrollbox4: tscrollbox;
   tbooleanedit1: tbooleanedit;
   tbooleanedit2: tbooleanedit;
   tbooleanedit3: tbooleanedit;
   tbooleanedit4: tbooleanedit;
   b_mirror: tbooleanedit;
   b_flip: tbooleanedit;
   b_rotate90: tbooleanedit;
   tlabel2: tlabel;
   bl9: trichbutton;
   tp_audiocd_pl: ttabpage;
   ddd_audioCDdevice2: tdropdownlistedit;
   trichbutton2: trichbutton;
   l_audiocd_capt: tlabel;
   tlabel5: tlabel;
   tp_audiocd: ttabpage;
   sg_cdplaylist: tstringgrid;
   s_audio_top: tspacer;
   b_repeat: trichbutton;
   function dirtowin(str : string) : string;
   //form
   procedure on_createform(const sender: TObject);
   procedure on_createdform(const sender: TObject);
   procedure on_showform(const sender: TObject);
   procedure on_resizevideo(const sender: TObject);
   procedure resizevideo(screenwidth, screenheight : integer);
   procedure on_resizeform(const sender: TObject);
   procedure on_childmouseevents(const sender: twidget; var ainfo: mouseeventinfoty);
   procedure on_show_about(const sender: TObject);
   procedure on_close_app(const sender: TObject);
   procedure on_terminatequery(var terminate: Boolean);
   //top buttons
   procedure on_minimize(const sender: TObject);
   procedure on_maximize(const sender: TObject);
   //move and resize timers
   procedure on_timer_move(const sender: TObject);
   procedure on_timer_resize(const sender: TObject);
   //autocorrect position
   procedure autocorrect_middle;
   procedure autocorrect_spl1_position(const sender: TObject);
   procedure showpanels;   
   //MAIN bottom buttons   
   procedure on_changewindowstyle(const sender: TObject);
   procedure on_fullscreen(const sender: TObject);
   procedure on_showlibrary(const sender: TObject);
   procedure on_showplaylist(const sender: TObject);
   //left panel
   procedure on_dobblevideo(const sender: TObject);
   procedure on_navigate_media(const sender: TObject);
   procedure on_navigate2(const sender: TObject);
   procedure on_show_settings(const sender: TObject);
   //settings
   procedure loadsettings;
   procedure savesettings;   
   //middle
   procedure on_hide_addons(const sender: TObject);
   procedure on_showtrackinfo(const sender: TObject);
   //file OPERATIONS
   function show_ask(const sender : tobject; capt,fname : string; ddd_visible, se_visible,yes_visible : boolean) : modalresultty;
   procedure rename_file(fold : string; updateplaylist, update_flv : boolean);
   Function MyRemoveDir(sDir : String) : Boolean; 
   procedure copyfiles(sl, sl2 : tstringlist; copy_ : boolean);
   //add to playlist
   procedure addfile(fcapt, url, emblem :string);
   procedure adddir(path : string);
   //file navigator
   procedure on_set_directory_finav(const sender: TObject; var avalue: msestring; var accept: Boolean);
   procedure on_go_home(const sender: TObject);
   procedure on_go_up(const sender: TObject);
   procedure on_rename_file_in_flv(const sender: TObject);
   procedure on_deletefile_flv(const sender: TObject);
   procedure on_reread_flv(const sender: TObject);
   procedure on_filenavigate(const sender: tcustomlistview; const index: Integer; var info: celleventinfoty);
   procedure on_addfilefromdisk(const sender: TObject);
   //last played
   procedure on_add_from_lastplayed(const sender: TObject);
   procedure on_add_file_from_last_tracks(const sender: TObject; var info: celleventinfoty);
   procedure on_clear_list_of_last_played(const sender: TObject);
   procedure on_delete_from_last_played(const sender: TObject);
   //my playlist
   procedure on_loadplaylists_list(const sender: TObject);
   procedure save_in_my_playlists(const wg : twidgetgrid; const se: tstringedit; fname : string);
   procedure on_rename_my_playlist(const sender: TObject);
   procedure on_delete_from_my_playlists(const sender: TObject);
   procedure on_add_myplaylist_To_Playlist(const sender: TObject);
   procedure on_dblclick_myplaylists(const sender: TObject;var info: celleventinfoty);
   procedure on_save_in_my_playlists(const sender: TObject);
   procedure on_rewrite_my_playlist(const sender: TObject);
   //radio and tv online
   procedure on_LOADRADIOtv(const sender: TObject);
   procedure loadradiotv(fname, prev : string; wg : twidgetgrid);
   procedure on_delete_chanel(const sender: TObject);
   procedure on_add_radio_chanel(const sender: TObject);
   procedure on_edit_chanel(const sender: TObject);
   procedure on_save_chanel(const sender: TObject);
   procedure on_add_radioTVchanel_to_playlist(const sender: TObject);
   procedure on_radioTVlist_cell_event(const sender: TObject; var info: celleventinfoty);
   //playlist
   procedure ON_SHOW_PL_POPUP(const sender: TObject);
   procedure findcurrentruck(_restore : boolean);
   //open
   procedure on_addfiles(const sender: TObject);
   procedure on_add_url(const sender: TObject);
   procedure on_openplaylist(const sender: TObject);
   procedure on_open_directory(const sender: TObject);
   //copy enter point
   procedure on_copyfile(const sender: TObject);
   //save
   procedure on_saveinMyPlayLists(const sender: TObject);
   procedure on_save_as_radio_or_tv_chanel(const sender: TObject);
   procedure on_save_urls_to_clipboard(const sender: TObject);
   procedure on_save_as__(const sender: TObject);
   procedure on_save_files_to__(const sender: TObject);
   procedure on_movefiles_to__(const sender: TObject);
   //rename file   
   procedure on_rename_file_in_playlist(const sender: TObject);
   //delete main procedure
   procedure on_deleteformplaylist_pm(const sender: TObject);
   //move in playlist
   procedure on_topTruckinPlaylist(const sender: TObject);
   procedure on_upTrackInPlaylist(const sender: TObject);
   procedure on_downTrackInPlaylist(const sender: TObject);
   procedure on_bottomTrauckinPlaylist(const sender: TObject);
   //sort and shuffle
   procedure on_shuffle(const sender: TObject);
   procedure on_sort_playlist(const sender: TObject);
   //clear
   procedure on_clearplaylist(const sender: TObject);
      
   
   
    procedure on_dblclick_on_playlist(const sender: TObject;
                   var info: celleventinfoty);
   //player events
   procedure on_changeVolumeEvent(const Sender: TObject; avolume: Integer);
   procedure onConnectingEvent(const Sender: TObject; msg: AnsiString);
   procedure on_EndOfTrackEvent(const Sender: TObject; msg: AnsiString);
   procedure on_ErrorEvent(const Sender: TObject; errormsg: AnsiString);
   procedure on_GetDebugMessage(const Sender: TObject; msg: AnsiString);
   procedure on_PauseEvent(const Sender: TObject; const apausing: Boolean; msg: AnsiString);
   procedure on_PlayingEvent(const Sender: TObject; position: Integer; length: Integer; msg: AnsiString);
   procedure on_StartPlayEvent(const Sender: TObject; videoW: Integer;videoH: Integer; msg: AnsiString);
   //player2 events
   procedure on_playingevent2(const Sender: TObject; position: Integer;length: Integer; msg: AnsiString);
   procedure on_startplayevent2(const Sender: TObject; videoW: Integer;videoH: Integer; msg: AnsiString);
   //play management
   procedure on_prev(const sender: TObject);
   procedure on_play(const sender: TObject);
   procedure on_stop(const sender: TObject);
   procedure on_next(const sender: TObject);
   
   procedure on_load_audio_1(const sender: TObject);
   procedure on_set_audio_1(const sender: TObject);
   procedure on_set_audio_0(const sender: TObject);

   //sliders and mute 
   procedure on_change_position(const sender: TObject);
   procedure ON_SETPOSITION(const sender: TObject; var avalue: realty;var accept: Boolean);
   procedure ON_SETVOLUME(const sender: TObject; var avalue: realty;var accept: Boolean);
   procedure ON_MUTE(const sender: TObject);
   procedure on_showVsetform(const sender: TObject);
   procedure on_setbrightness(const sender: TObject; var avalue: realty;
                   var accept: Boolean);
   procedure on_setsaturation(const sender: TObject; var avalue: realty;
                   var accept: Boolean);
   procedure on_setcontrast(const sender: TObject; var avalue: realty;
                   var accept: Boolean);
   procedure on_sethue(const sender: TObject; var avalue: realty;
                   var accept: Boolean);
   procedure on_setgamma(const sender: TObject; var avalue: realty;
                   var accept: Boolean);
   procedure  on_setbalance(const sender: TObject; var avalue: realty;var accept: Boolean);

   procedure on_reset_video_options(const sender: TObject);
   procedure on_set_resolution(const sender: TObject);

   procedure on_set_equalizer(const sender: TObject);   
   procedure on_set_equalizer_row(const sender: TObject; var avalue: realty;
                   var accept: Boolean);
   procedure on_cancel_equalizer(const sender: TObject);
   procedure on_set_audioN(const sender: TObject; var avalue: Boolean;
                   var accept: Boolean);
   procedure on_set_volnorm(const sender: TObject; var avalue: Boolean;
                   var accept: Boolean);  
   procedure on_set_karaoke(const sender: TObject; var avalue: Boolean;
                   var accept: Boolean);
   procedure on_set_extrastereo(const sender: TObject; var avalue: Boolean;
                   var accept: Boolean);
   procedure on_set_vectors(const sender: TObject; var avalue: Boolean;
                   var accept: Boolean);
   procedure on_set_videoeffect(const sender: TObject; var avalue: Boolean;
                   var accept: Boolean);
   procedure restart_play;
   procedure on_maintimer(const sender: TObject);
   procedure on_open_stop_dvd(const sender: TObject);
   procedure on_dvd_menu(const sender: TObject);
   procedure on_dvd_left(const sender: TObject);
   procedure on_dvd_right(const sender: TObject);
   procedure on_dvd_up(const sender: TObject);
   procedure on_dvd_down(const sender: TObject);
   procedure on_dvd_select(const sender: TObject);
   procedure on_dvdpause(const sender: TObject);
   procedure on_set_dvd_iso_path(const sender: TObject; var avalue: Boolean;
                   var accept: Boolean);
                   
   // TV - TUNER
   procedure loadtvtuner;                
   procedure on_dblclickOnTVlist(const sender: TObject;
                   var info: celleventinfoty);
   procedure on_addchanell(const sender: TObject);
   procedure on_deletechanell(const sender: TObject);
   procedure on_setchanneltv(const sender: TObject);
   procedure SaveChannel();
   procedure on_set_webcam2(const sender: TObject; var avalue: msestring;
                   var accept: Boolean);
   procedure on_updatedivices2(const sender: TObject);
   procedure on_setplayplaylist_up_down(const sender: TObject);
   procedure on_setrepeatplaylist(const sender: TObject);
   procedure on_getpreview_pl(const sender: TObject);
   procedure getpreview(fname : string);
   procedure on_showdubble(const sender: TObject);
   procedure on_deactivateform(const sender: TObject);
   
   procedure loadshemas;
   procedure loadicons;
   procedure on_timer_animate(const sender: TObject);
   procedure on_screenshot(const sender: TObject);
   procedure on_set_dvd_device(const sender: TObject; var avalue: Boolean;
                   var accept: Boolean);
   procedure on_set_cdrom_device3(const sender: TObject; var avalue: msestring;
                   var accept: Boolean);
   procedure on_keyup_form(const sender: twidget; var ainfo: keyeventinfoty);
   procedure on_mousewheelform(const sender: twidget;
                   var ainfo: mousewheeleventinfoty);
   procedure on_get_icon(const sender: TObject; const ainfo: fileinfoty;
                   var imagelist: timagelist; var imagenr: Integer);
   procedure on_show_flv_popup(const sender: TObject);
   procedure on_show_flv_menu(const sender: tcustommenu);
   procedure on_open_dir_pm(const sender: TObject);

   procedure on_last_played_change(const sender: tfilechangenotifyer;
                   const info: filechangeinfoty);
   procedure loadlast(dir : string);
   procedure on_createitemflv(const sender: tcustomitemlist;
                   var item: tlistedititem);
   procedure on_ckeckfile(const sender: TObject;
                   const streaminfo: dirstreaminfoty;
                   const fileinfo: fileinfoty; var accept: Boolean);
   procedure on_listread_flv(const sender: TObject);
   
   procedure readlist(dir : string);
   procedure on_fl_event(const sender: TObject; var info: celleventinfoty);
   procedure on_getpreview_flv(const sender: TObject);
   procedure on_getpreview_last(const sender: TObject);
   procedure on_player_preview_startplay(const Sender: TObject; videoW: Integer;
                   videoH: Integer; msg: AnsiString);
   procedure on_player_info_tartplay_event(const Sender: TObject;
                   videoW: Integer; videoH: Integer; msg: AnsiString);
   procedure on_beforedragbegin_pl(const asender: TObject; const apos: pointty;
                   var adragobject: tdragobject; var processed: Boolean);
   procedure on_beforedragdrop_pl(const asender: TObject; const apos: pointty;
                   var adragobject: tdragobject; var processed: Boolean);
   procedure on_beforedragover_pl(const asender: TObject; const apos: pointty;
                   var adragobject: tdragobject; var accept: Boolean;
                   var processed: Boolean);
   procedure on_dragbegin(const asender: TObject; const apos: pointty;
                   var adragobject: tdragobject; var processed: Boolean);
   procedure on_dragdrop(const asender: TObject; const apos: pointty;
                   var adragobject: tdragobject; var processed: Boolean);
   procedure on_dragover(const asender: TObject; const apos: pointty;
                   var adragobject: tdragobject; var accept: Boolean;
                   var processed: Boolean);
   function get_mainwindow : tcustommseform;                
   procedure on_dblclick_audiocd(const sender: TObject;
                   var info: celleventinfoty);
   procedure on_getaudiocdplaylist(const Sender: TObject; msg: AnsiString);
 end;
var
  mainfo: tmainfo;
  _formt, _forml, _formw,_formh : integer; //for fullscreen (windows hack)
  _formws,_formhs : integer; //for change style
  moveXX, moveYY : integer;
  __showsetsfofirst : boolean = true;
  __showset2fofirst : boolean = true;
  __showaboutfofirst: boolean = true;
  __showfirstresize : boolean = true;
  __continueplay : boolean = false;
  __continueplaypos : integer;
  __lastviewstyle : byte; // for close/open library due changing style
  apppath, conffile, playlistspath,langspath, radiotvpath, shemepath, shemeimagepath : string;
  spl1_left : real;
  _cursor : string;//current track in playlist;
  audioN  : byte; //external (1) internal(0) audio 
  n_animate : byte = 0; //number if image due animate
  mainfoframewidth : integer;
  //tv-tuner
  CFGChannelsName : string;
  SC : Tinifile;
  CountSelectionGrid : Integer; 
  B_USEONE : BOOLEAN;
  filemagic: string;  
  svideoanchor : anchorsty;
  mp3f : TMP3File;
  //
      
implementation
uses
  main_mfm, top, bottom, playlist, sets, askform, msefileutils, msesysintf, viewform,
  playlists, functions, chanelform, copyform, mseact,aboutform, process,  
  set2form, msegridsglob, previewform, dubbleform, loadform, editshemeform, infoform,
  msekeyboard, {$ifdef linux}baseunix, unix,  magic4lazarus, {$endif} cdrom,
  mseformatpngread, mseformatjpgread, mseformatxpmread, fileinfo, 
  mseformatbmpicoread,mseformatpnmread, mseformattgaread, mseucs2toru;

function tmainfo.dirtowin(str : string) : string;
var i : integer;
begin
  IF LENGTH(STR) > 0 then
  {$ifdef linux} 
  if str[length(str)] <> '/' then str := str + '/';
  {$endif} 
  {$ifdef windows} 
  IF str[1] = '/' THEN DELETE(str,1,1);
  IF LENGTH(STR) > 0 THEN 
  for i := 1 to length(str) do 
    if str[i] = '/' then str[i] := '\';  
  if length(str) > 0 then
  if str[length(str)] <> '\' then str := str + '\';
  {$endif} 
  result := str;
end;

function whoami : string;
var  p : tprocess;
    sl : tstringlist;
begin
 sl := tstringlist.create;
 p := tprocess.create(nil);

 p.commandline := 'whoami'; 
 p.Options := p.Options +[poWaitOnExit,poUsePipes];
 p.execute;
 sl.LoadFromStream(p.Output);
 result := trim(sl.text);
 p.free;
 sl.free;
end;

function HTTPEncode(const AStr: String): String;
const
  NoConversion = ['A'..'Z','a'..'z','*','@','.','_','-'];
var
  Sp, Rp: PChar;
begin
  SetLength(Result, Length(AStr) * 3);
  Sp := PChar(AStr);
  Rp := PChar(Result);
  while Sp^ <> #0 do
  begin
    if Sp^ in NoConversion then
      Rp^ := Sp^
    else
      if Sp^ = ' ' then
        Rp^ := '+'
      else
      begin
        FormatBuf(Rp^, 3, '%%%.2x', 6, [Ord(Sp^)]);
        Inc(Rp,2);
      end;
    Inc(Rp);
    Inc(Sp);
  end;
  SetLength(Result, Rp - PChar(Result));
end;

function MyEncodeUrl(source:string):string;
 var i:integer;
 begin
   result := '';
   for i := 1 to length(source) do
       if not (source[i] in ['A'..'Z','a'..'z','0','1'..'9','-','_','~','.']) 
          then result := result + '%'+inttohex(ord(source[i]),2) else result := result + source[i];
 end;
 
 // Convert URLEncoded string to utf8 string
 //http://forum.lazarus.freepascal.org/index.php?topic=9184.0
function URLDecode(const s: String): String;
var
   sAnsi: String;
   sUtf8: String;
   sWide: WideString;

   i, len: Cardinal;
   ESC: string[2];
   CharCode: integer;
   c: char;
begin
   sAnsi := PChar(s);
   SetLength(sUtf8, Length(sAnsi));
   i := 1;
   len := 1;
   while (i <= Cardinal(Length(sAnsi))) do begin
      if (sAnsi[i] <> '%') then begin
         if (sAnsi[i] = '+') then begin
            c := ' ';
         end else begin
            c := sAnsi[i];
         end;
         sUtf8[len] := c;
         Inc(len);
      end else begin
         Inc(i);
         ESC := Copy(sAnsi, i, 2);
         Inc(i, 1);
         try
            CharCode := StrToInt('$' + ESC);
            c := Char(CharCode);
            sUtf8[len] := c;
            Inc(len);
         except end;
      end;
      Inc(i);
   end;
   Dec(len);
   SetLength(sUtf8, len);

   sWide := UTF8Decode(sUtf8);
   len := Length(sWide);

   Result := sWide;
end;
// ======================================================= 
//               FORM
// ======================================================= 
procedure tmainfo.on_createform(const sender: TObject);
begin
  __showsetsfofirst := false;
  //application.createform(tloadfo,loadfo);
  loadfo.l_.caption := 'Loading forms ...'; loadfo.update;
  application.createform(tviewfo,viewfo);
  application.createform(ttopfo,topfo);
  application.createform(tbottomfo,bottomfo);
  application.createform(tplaylistfo,playlistfo);
  application.createform(tsetsfo,setsfo);
  application.createform(tset2fo,set2fo);
  application.createform(taskfo,askfo);
  application.createform(tchanelfo,chanelfo);
  application.createform(tcopyfo,copyfo);
  application.createform(taboutfo,aboutfo);
  application.createform(tpreviewfo,previewfo);
  application.createform(tdubblefo,dubblefo);
  application.createform(teditshemefo,editshemefo);
  application.createform(tinfofo,infofo);
  application.createform(tfileinfofo,fileinfofo);
  
  {$ifdef linux}  //work only from second time (in open box ???)
  loadfo.l_.caption := 'Set form options ...'; loadfo.update;
  playlistfo.optionswindow := [wo_popupmenu,wo_noframe,wo_notaskbar, wo_sysdnd];
  topfo.optionswindow := [wo_popup,wo_notaskbar];
  setsfo.optionswindow := [wo_popupmenu,wo_noframe,wo_notaskbar];//[wo_popup,wo_notaskbar];
  set2fo.optionswindow := [wo_popup,wo_notaskbar];
  askfo.optionswindow  := [wo_popup,wo_notaskbar];
  bottomfo.optionswindow := [wo_popup,wo_notaskbar];
  chanelfo.optionswindow := [wo_popup,wo_notaskbar];
  copyfo.optionswindow   := [wo_popup,wo_notaskbar];
  aboutfo.optionswindow  := [wo_popup,wo_notaskbar];
  previewfo.optionswindow  := [wo_popup,wo_notaskbar];
  dubblefo.optionswindow   := [wo_popup,wo_notaskbar];
  infofo.optionswindow   := [wo_popup,wo_notaskbar];
  viewfo.optionswindow   := [wo_popupmenu,wo_noframe,wo_notaskbar];
  editshemefo.optionswindow   := [wo_dialog,wo_noframe,wo_notaskbar];//[wo_popup,wo_notaskbar];
  fileinfofo.optionswindow   := [wo_popup,wo_notaskbar];//[wo_popupmenu,wo_noframe,wo_notaskbar];
  
  {$endif}  
  loadfo.l_.caption := 'Set component options ...'; loadfo.update;
  tw_main.height := tw_main.height + tw_main.tab_size + 2; 
  tw_addons.height := tw_addons.height + tw_addons.tab_size + 2; 
  tw_playlist.height := tw_playlist.height + tw_playlist.tab_size + 2; 
  topfo.height := s_top.height;
  //configuration file
  {$ifdef linux}_cursor := '♫';{$endif}
  {$ifdef mswindows}_cursor := '>';{$endif}
  setsfo.l_alsmplayer_version.caption := player.version;   

  b_dubble.parentwidget := dubblefo;
  dubblefo.width  := b_dubble.width;
  dubblefo.height := b_dubble.height;
  b_dubble.top := 0;
  b_dubble.left:= 0;
  tw_sets.parentwidget := set2fo.container;
  set2fo.trichbutton1.bringtofront;
  tw_sets.anchors := [an_top,an_bottom];
  
  loadfo.l_.caption := 'Set configuration ...'; loadfo.update; 

//  forcedirectories(sys_getuserhomedir + '/.Almin-Soft/xelplayer');
//  conffile := dirtowin(sys_getuserhomedir + '/.Almin-Soft/xelplayer/') + 'xelplayer15.conf';
//  playlistspath := dirtowin(sys_getuserhomedir + '/.Almin-Soft/xelplayer/playlists/');
  //vars
  player.volume := 50;
  sl_volume.value := player.volume / 100;
  pb_volume.value := player.volume / 100;
  //pathes
  apppath := dirtowin(extractfilepath(sys_getapplicationpath));  
  conffile := apppath + 'xelplayer15.conf';
  playlistspath := dirtowin(apppath + '/playlists/');
  forcedirectories(playlistspath);
  shemepath := dirtowin(apppath + 'shemas/');
  forcedirectories(shemepath);
  shemeimagepath := dirtowin(apppath + 'shemas/images/');
  langspath := dirtowin(apppath + 'langs/');
  forcedirectories(shemeimagepath);
  radiotvpath := apppath;

  //tv-tuner
  CFGChannelsName := apppath + '/channelstv.ini';
  loadfo.l_.caption := 'load TVTuner settings ...'; loadfo.update; 
  loadtvtuner;  //  loadtv and loadradio are in loadsettings
  {
  if not setsfo.be_useonecopy.value then 
    begin
      if paramstr(1) > '' then 
        begin
          addfile(extractfilename(paramstr(1)), paramstr(1), '');
          player.play;
        end
    end;
  }
  player.getavaliableoutputs;
  spl1_left := spl1.left / width; //important before on_showplaylist here!
end;

procedure tmainfo.on_createdform(const sender: TObject);
var fs : tsearchrec;
begin
  loadfo.l_.caption := 'Prepare interface ...'; loadfo.update; 
  on_navigate2(b_l_3);
  on_navigate2(b_l_2);
  on_navigate_media(bl1);
  on_hide_addons(sender);

  spl1_left := spl1.left / width; //important before on_showplaylist here!

  loadshemas; 
  loadicons; 

  //on_loadradiotv(trichbutton20);
  //on_loadradiotv(trichbutton21); //loaded  while load settings
  //on_loadplaylists_list(sender); -- this need not because read my playlists is 
                              //maked when dd_playlistsdir.value is set (onchange event)
  //dubblefo.visible := false;
  loadfo.l_.caption := 'Load settings ...'; loadfo.update; 
  loadsettings;
  on_showplaylist(sender); //only after loadsettings (exec on_dubble form click);
  setsfo.on_set_lang(setsfo.trichbutton5);// not in loadsettings
  loadfo.hide;
  mainfo.visible := true;
end;

procedure tmainfo.on_showform(const sender: TObject);
begin
  if fo_minimized in mainfo.options
   then mainfo.options := mainfo.options - [fo_minimized]
end;

// ======================================================= 
//               resize video out
// ======================================================= 
procedure tmainfo.resizevideo(screenwidth, screenheight : integer);
var r : real;
 rect : rectty; 
    i : integer;
begin
  if screenheight = 0 then exit;  
  if screenwidth = screenheight  // autocorrect acpect
    then begin                   // mplayer make it automatically
           screenwidth := 640;   // but this output alsmplayer 
           screenheight:= 480;   // not catch 
         end;

  if player.videoeffect_rotate90 then 
     begin
       i := screenwidth;
       screenwidth := screenheight;
       screenheight := i;
     end;
    
  mainfo.ww_video.left := 0;
  mainfo.ww_video.top := 0;

  viewfo.ww_video.left := 0;
  viewfo.ww_video.top := 0;

  if ber1.value then 
  r := screenwidth / screenheight;
  if ber2.value then r := 4 / 3;
  if ber3.value then r := 16 / 9;


//  if viewfo.visible then 
 if b_windowstyle.tag = 1 then 
  begin
//  writeln('viewfo resize');
  viewfo.ww_video.height := viewfo.s_video.HEIGHT; 
  viewfo.ww_video.bounds_cx  := round(viewfo.ww_video.bounds_cy * r);

  if viewfo.ww_video.bounds_cx <= viewfo.s_video.bounds_cx
     then viewfo.ww_video.left := (viewfo.s_video.bounds_cx - viewfo.ww_video.bounds_cx) div 2
     else begin
    	    viewfo.ww_video.bounds_cx := viewfo.s_video.bounds_cx;
 	        viewfo.ww_video.bounds_cy := round(viewfo.ww_video.bounds_cx / r);
    	    viewfo.ww_video.top{mainfo.ww_video.bounds_y} := (viewfo.s_video.bounds_cy- viewfo.ww_video.bounds_cy) div 2;
	      end;

  rect.cx := viewfo.ww_video.viewport.cx; //important! resize mainfo.ww_video bug fix
  rect.cy := viewfo.ww_video.viewport.cy; 
  end
  else
  begin
//  writeln('mainfo resize');
  mainfo.ww_video.height := mainfo.s_video.HEIGHT; 
  mainfo.ww_video.bounds_cx  := round(mainfo.ww_video.bounds_cy * r);

  if mainfo.ww_video.bounds_cx <= mainfo.s_video.bounds_cx
     then mainfo.ww_video.left{mainfo.ww_video.bounds_x} := (mainfo.s_video.bounds_cx - mainfo.ww_video.bounds_cx) div 2
     else begin
    	    mainfo.ww_video.bounds_cx := mainfo.s_video.bounds_cx;
 	        mainfo.ww_video.bounds_cy := round(mainfo.ww_video.bounds_cx / r);
    	    mainfo.ww_video.top{mainfo.ww_video.bounds_y} := (mainfo.s_video.bounds_cy- mainfo.ww_video.bounds_cy) div 2;
	      end;

	         
  rect.cx := mainfo.ww_video.viewport.cx; //important! resize mainfo.ww_video bug fix
  rect.cy := mainfo.ww_video.viewport.cy; 
//  writeln( mainfo.ww_video.top);
//  writeln( mainfo.ww_video.left);
//  writeln( mainfo.ww_video.height);
//  writeln( mainfo.ww_video.width);
  end;

//  writeln('call viewport');
  if player.playing then       
       if (not setsfo.ber_on_desktop.value)and (player.videooutput <> 'null')
        then begin 
             //if b_windowstyle.tag = 0 
                //then 
               
               if b_windowstyle.tag = 1 then 
                 begin
                 viewfo.visible := true;
                 viewfo.ww_video.visible := true;
                 end
               else 
                 mainfo.ww_video.visible := true;

                //else mainfo.b_animate.visible := true;                
             //s_video.color := cl_black;
             end;
  
  if viewfo.visible then viewfo.ww_video.update
    else 
    mainfo.ww_video.update;
end;

procedure tmainfo.on_resizevideo(const sender: TObject);
begin
  if player <> nil then
   case player.mode of
   __tv, __webcamera : resizevideo(640,480);
   else resizevideo(player.currenttrack.video.width, player.currenttrack.video.height);
   end;//case
end;

procedure tmainfo.on_resizeform(const sender: TObject);
begin
  on_showdubble(sender);
  s_btn_center.left := width div 2 - s_btn_center.width div 2;
  if __showfirstresize 
     then __showfirstresize := false
     else begin
          if spl1.anchors = [an_right] 
             then spl1.left := round(spl1_left * width)//s_left.width + (width - s_left.width) div 2
             else spl1.left := s_left.width;
          end;
     
     
    // spl1.left := round(spl1_left * width);
end;

procedure tmainfo.on_childmouseevents(const sender: twidget;var ainfo: mouseeventinfoty);
begin
 //s_top.frame.caption := sender.name;
 if b_fullscreen.tag = 1 then       //if fullscreen
 if ainfo.eventkind = ek_mousemove then
          case b_library.tag of
          0 : if ((ainfo.pos.y > bottomfo.TOP - mainfo.top) or (ainfo.pos.y < topfo.height{ + mainfo.top}))
               then begin
                    if b_playlist.tag = 0 then 
                     if (sender <> s_video)and (mainfo.ww_video <> sender)
                     and (sender <> mainfo.im_emblem) and (sender <> mainfo.b_animate) then exit; 
                    showpanels;
                    end
               ELSE begin 
                    bottomfo.VISIBLE := FALSE;
                    topfo.VISIBLE := FALSE;
                    end;
          1 : showpanels;
          end;//case

  if (ainfo.eventkind = ek_buttonpress)and(ss_double in ainfo.shiftstate) then //dbl click to top
  BEGIN
   if (s_top = sender) then if b_maximize.enabled then on_maximize(sender);
   if (s_video = sender) or (mainfo.ww_video = sender)
      or (mainfo.b_animate = sender) or (mainfo.im_emblem = sender)
   then begin
        if b_fullscreen.tag = 0 
        then begin
             on_fullscreen(sender);
             if b_playlist.tag = 0 then on_showplaylist(sender);
             end
        else if b_library.tag = 1 then on_showlibrary(sender) 
                                  else on_fullscreen(sender);
        end;
  END
  else
  if (ainfo.eventkind = ek_buttonpress) then //move
  BEGIN
   if sender = sl_volume then
     sl_volume.value := (gui_getpointerpos.x - left - s_bottom_right.left - sl_volume.left - mainfoframewidth - mainfo.s_bottom.frame.framewidth) / sl_volume.width
   else
   if sender = sl_position then
     sl_position.value := (gui_getpointerpos.x - left - sl_position.left - mainfoframewidth - mainfo.s_bottom.frame.framewidth) / sl_position.width
   else
   if (mainfo.ww_video = sender)or(l_video_inf = sender)or(l_video_inf2 = sender)or
      {(sender = sb_left) or} (sender.classname = 'tspacer') or 
      {(sender.classname = 'twidgetgrid') or} //leave for drag and drop
      (mainfo.b_animate = sender)or(mainfo.im_emblem = sender)
    then 
    begin
       moveXX := gui_getpointerpos.x - left;
       moveYY := gui_getpointerpos.y - top;
       timer_move.enabled := true; 
    end
   else
   if (tdockhandle1 = sender) then //resize
    begin
       moveXX := gui_getpointerpos.x - width;
       moveYY := gui_getpointerpos.y - height;
       timer_resize.enabled := true;
    end;
  END;
  if (ainfo.eventkind = ek_buttonrelease) then //cancel move/resize
   begin
   if timer_move.enabled then timer_move.enabled := false;
   if timer_resize.enabled then timer_resize.enabled := false;
   end;           
end;

procedure tmainfo.on_show_about(const sender: TObject);
begin
  if __showaboutfofirst
  then begin
       __showaboutfofirst := false;
       aboutfo.left := mainfo.left + (mainfo.width - aboutfo.width) div 2;
       aboutfo.top  := mainfo.top + (mainfo.height - aboutfo.height) div 2;       
       end;
  aboutfo.bringtofront;
  aboutfo.show;
end;

procedure tmainfo.on_close_app(const sender: TObject);
begin
  application.terminate;
end;

procedure tmainfo.on_terminatequery(var terminate: Boolean);
begin
  if setsfo.be_ask_forexit.value then
  if show_ask(mainfo,'Выйти из XELPLAYER ?','',false,false,true) <> mr_ok then
     begin 
       terminate := false; 
       exit; 
     end;
  if b_windowstyle.tag = 1 
     then player.sendcommandtomplayer('quit')
     ELSE player.stop; 
  SAVESETTINGS;
  //save_in_my_playlists(wg_last, se_url_last, dirtowin(setsfo.dd_playlistsdir.value) + '__LAST_PLAYED__');
end;

// ======================================================= 
//               top buttons
// ======================================================= 
procedure tmainfo.on_minimize(const sender: TObject);
begin
  if fo_minimized in mainfo.options = false
   then mainfo.options := mainfo.options + [fo_minimized];
  dubblefo.visible := false;
  if b_fullscreen.tag = 1 
  then begin
       bottomfo.visible := false;
       topfo.visible := false;
       end;
end;

procedure tmainfo.on_maximize(const sender: TObject);
begin
  if b_fullscreen.tag = 1  
  then begin
       on_fullscreen(sender);
       exit;
       end;
    s_btn_center.left := 0;//this hack for prevent breaking resize of bottom panel
  
  if fo_maximized in mainfo.options
    then begin 
         mainfo.options := mainfo.options - [fo_maximized];
         tdockhandle1.visible := true;
         end
    else begin
         mainfo.options := mainfo.options + [fo_maximized];
         tdockhandle1.visible := false;
         end;
  application.processmessages;
  on_resizevideo(sender);
end;

// ======================================================= 
//          move and resize timers
// ======================================================= 
procedure tmainfo.on_timer_move(const sender: TObject);
begin
  left := gui_getpointerpos.x - moveXX;
  top := gui_getpointerpos.y - moveYY;
  on_showdubble(sender);
end;

procedure tmainfo.on_timer_resize(const sender: TObject);
begin
  width := gui_getpointerpos.x - moveXX;
  height := gui_getpointerpos.y - moveYY;
end;
// ======================================================= 
//       autocorrect position
// ======================================================= 
procedure tmainfo.autocorrect_middle;
begin
  s_main.sendtoback;
  if (b_fullscreen.tag = 1) and (b_library.tag = 0)
      then s_middle.anchors := []
      else begin
             s_middle.anchors := [an_top,an_bottom];
             s_middle.top := s_top.height + mainfoframewidth;
             s_middle.height := mainfo.height - s_top.height - s_bottom.height - mainfoframewidth * 2;
           end;
end;

procedure tmainfo.autocorrect_spl1_position(const sender: TObject);
begin 
  if s_main.width > 1 then
  begin
  if s_middle.parentwidget = mainfo.container
  then begin 
       spl1_left := spl1.left / width;
       on_resizevideo(sender);
       end
  else begin
       spl1_left := spl1.left / playlistfo.width;
       end;
  end;
  //writeln('spl1_left=',spl1_left);
end;

procedure tmainfo.showpanels;
begin 
  topfo.width := mainfo.width;
  topfo.top := mainfo.top;//0;
  topfo.left := mainfo.left;// if 0 then may be problems with multi screen systems;
  if b_fullscreen.tag = 1 then topfo.visible := true;
  topfo.bringtofront;
     
  bottomfo.width := topfo.width;    //prevent screen resolution cachge
  bottomfo.top := mainfo.top + mainfo.height - bottomfo.height;
  bottomfo.left := mainfo.left;// if 0 then may be problems with multi screen systems;
  if b_fullscreen.tag = 1 then bottomfo.VISIBLE := TRUE; 
  bottomfo.bringtofront;    // bug in windows (panel not sets to front
  
  if b_fullscreen.tag = 0 
  then begin
         bottomfo.VISIBLE := false; 
         topfo.visible := false;
       end;
end;
// ======================================================= 
//         MAIN BOTTOM BUTTONS
// ======================================================= 
procedure tmainfo.on_changewindowstyle(const sender: TObject);
var bb : boolean;
    r : real;
begin
 bb := false;
 if (player.playing)and(player.havevideostream) then 
   begin
     bb := true;
     __continueplay := player.pausing;
     __continueplaypos := player.position;
     r := sl_position.value;
     on_stop(sender);
   end;
 
 writeln('b_windowstyle=',b_windowstyle.tag);
 
 autocorrect_spl1_position(sender);

 case b_windowstyle.tag of 
 0:begin
   _formws := mainfo.width;
   _formhs := mainfo.height;
   __lastviewstyle := b_playlist.tag;
   if fo_maximized in mainfo.options
     then mainfo.options := mainfo.options - [fo_maximized]; 
   if b_library.tag = 0 then on_showlibrary(sender); //important
   s_middle.parentwidget := playlistfo.container;
   s_middle.height := playlistfo.height - playlistfo.s_top.height - mainfoframewidth * 2;
   s_btn_center.visible := false;    //this hack for prevent breaking resize of bottom panel
   mainfo.bounds_cymin := s_bottom.height + mainfoframewidth * 2;
   mainfo.height := s_bottom.height + mainfoframewidth * 2;
   s_top.visible := false;
   mainfo.bounds_cxmin := 500;
   mainfo.width := 500;
   s_btn_center.visible := true;   //end of hack
   playlistfo.top := mainfo.top + mainfo.height + mainfoframewidth * 2;
   playlistfo.width := mainfo.width  + (mainfo.width div 2);
   playlistfo.height := 300;
   playlistfo.left := mainfo.left;
   playlistfo.tdockhandle1.bringtofront;
   s_bottom.bringtofront;
   tw_playlist.left := tw_playlist.left + 3;
   tw_playlist.height := tw_playlist.height + 2;
   b_library.onexecute := @playlistfo.on_show_media;
   playlistfo.on_show_media(sender);
   b_maximize.enabled := false;
   tdockhandle1.visible := false;
   b_close2.visible := true;
   b_windowstyle.tag := 1;
   im_emblem.parentwidget := viewfo.s_video;
   b_animate.parentwidget := viewfo.s_video;

   s_video.height := 0;
   s_left.height := s_middle.height;
   player.display := viewfo.ww_video;
   viewfo.top := mainfo.top + mainfo.height + mainfoframewidth * 2;
   viewfo.left := mainfo.left;
   end;
 1:begin
   if playlistfo.b_showmedia.tag = 1 then playlistfo.on_show_media(sender);
   s_middle.parentwidget := mainfo.container;
   mainfo.height := _formhs;//s_top.height + s_bottom.height + 300;
   mainfo.bounds_cymin := 350;   
   autocorrect_middle;
   playlistfo.visible := false;
   b_library.onexecute := @on_showlibrary;//enabled := true;
   tdockhandle1.visible := true;
   b_maximize.enabled := true;
   s_top.visible := true;
   b_close2.visible := false;
   tw_playlist.left := tw_playlist.left - 3;
   tw_playlist.height := tw_playlist.height - 2;
   
   b_windowstyle.tag := 0;
   mainfo.width := _formws;//600;
   
   if mainfo.spl1.anchors = [an_right] 
      then mainfo.spl1.left := round(main.spl1_left * width)//s_left.width + (width - s_left.width) div 2
      else mainfo.spl1.left := mainfo.s_left.width;
//   spl1.left := s_left.width + (width - s_left.width) div 2;
   
   viewfo.visible := false;
   {
   s_video.top := 0;
   s_video.parentwidget := s_middle;
   s_video.anchors := svideoanchor;
   s_video.width := s_left.width;
   s_video.height := s_left.width div 2;
   }
   s_video.height := s_left.width div 2;
   s_left.top := s_video.height;
   s_left.height := s_middle.height - s_video.height;
   im_emblem.parentwidget := mainfo.s_video;
   b_animate.parentwidget := mainfo.s_video;

//   s_left.linktop := s_video;
   player.display := ww_video;
   application.processmessages;
//   player.display := nil;
   if __lastviewstyle = 1 then  on_showplaylist(sender);
   end;
 end;//case
 on_showdubble(sender);
 
 if bb then
   begin
     sl_position.value := r;
     if not __continueplay then 
      begin 
      	  if tw_main.activepage = tp_dvd then on_open_stop_dvd(sender)
      	  else
      	  if tw_main.activepage = tp_TUNER then 
      	     begin
      	     player.OpenTV();
      	     __continueplay := false;
      	     end
	      else 
	      if tw_main.activepage = TP_WEBCAM then 
	         begin
	         player.openwebcamera;
	         __continueplay := false;
	         end
	      else player.play(__continueplaypos);
	  end;
   end;
   
end;

procedure tmainfo.on_fullscreen(const sender: TObject);
begin
  if b_windowstyle.tag = 1 
  then BEGIN
       player.sendcommandtomplayer('vo_fullscreen');
       mainfo.bringtofront;
       exit;
       END;
       
  case b_fullscreen.tag of 
  0 : begin  
         {$ifdef mswindows}
         _formt := top;
         _forml := left;
         _formh := height;       
         _formw := width; 
         {$endif}
         
         mainfo.frame.framewidth := 0;
         mainfo.invalidate;
         mainfo.options := mainfo.options + [fo_fullscreen] - [fo_defaultpos];
         application.processmessages;
         mainfo.update;
         
         topfo.left := mainfo.left;// if 0 then may be problems with multi screen systems;
         topfo.top := 0;
         topfo.width := mainfo.width;
         s_top.parentwidget := topfo;
         
         bottomfo.height := s_bottom.height + mainfoframewidth * 2;
         bottomfo.left  := mainfo.left;// if 0 then may be problems with multi screen systems;
         bottomfo.width := mainfo.width;
         bottomfo.top   := mainfo.height - bottomfo.height;
         s_bottom.parentwidget := bottomfo;
         s_bottom.top := mainfoframewidth;
         tdockhandle1.visible := false;
         b_windowstyle.enabled := false;
         b_fullscreen.tag := 1;
         if b_library.tag = 1 
           then  begin
                 bottomfo.visible := true;
                 topfo.visible := true;
                 end;
         b_fullscreen.imagenr := 7; 
         b_minimize.enabled := false;
      end;
  1 : begin 
         s_btn_center.left := 0; //bug in linux while resize
         mainfo.invalidate;
         mainfo.options := mainfo.options - [fo_fullscreen] + [fo_defaultpos];
         {$ifdef mswindows}
         height := _formh;            //bug in windows 
         top := _formt;               //return sizes and position;
         left := _forml;              //need to check it
         width := _formw;
         {$endif}
         application.processmessages;
         mainfo.update;
         mainfo.frame.framewidth := mainfoframewidth;
         s_top.parentwidget := mainfo.container;
         s_top.top := 0;//mainfoframewidth is wrong decission
         s_bottom.parentwidget := mainfo.container;
         s_bottom.top := height - s_bottom.height - mainfoframewidth;
         tdockhandle1.visible := true;
         b_windowstyle.enabled := true;
         bottomfo.visible := false;
         topfo.visible := false;
         b_fullscreen.tag := 0;
         b_fullscreen.imagenr := 6; 
         b_minimize.enabled := true;
      end; 
  end; //case
  autocorrect_middle;
end;

procedure tmainfo.on_showlibrary(const sender: TObject);
begin
 if b_windowstyle.tag = 1 
  then begin
       //if not playlistfo.visible then playlistfo.b_showmedia.tag := 1 ;
       playlistfo.visible := true;
       playlistfo.bringtofront;
       playlistfo.on_show_media(sender);
       exit;
       end
  else
  case b_library.tag of
  1 :on_showplaylist(sender);
  0 :begin 
     b_library.tag := 1;
     b_playlist.tag := 0;

     if spl1.anchors = [an_left] then spl1.left := s_left.width;
             
     spl1.linkleft := s_main;
     s_video.width := s_left.width;
     s_video.height := s_video.width div 2;
     s_video.anchors := [an_left,an_top];
     if spl1.left < s_video.width 
       then spl1.left := s_left.width + (width - s_left.width) div 2;
     if b_fullscreen.tag = 1
     then begin 
          autocorrect_middle;
          showpanels;
          end;
     on_resizevideo(sender);
             s_left.visible := true;
             s_main.visible := true;
             s_playlist.visible := true;
             spl1.visible := true;
     end;
  end;//case
  try
  on_showdubble(sender);
  s_video.setfocus;
  except
  end;  
end;

procedure tmainfo.on_showplaylist(const sender: TObject);
begin
  //dubblefo.visible := false;
  b_library.tag := 0;
  if b_windowstyle.tag = 1 
     then playlistfo.visible := not playlistfo.visible
     else case b_playlist.tag of
          0: begin
             spl1.linkleft := nil;
             s_video.anchors := [];
             b_playlist.tag := 1;
             s_left.visible := false;
             s_main.visible := false;
             s_playlist.visible := false;
             spl1.visible := false;
             end;
          1: begin
             s_video.anchors := [an_left,an_right];
             spl1.linkleft := s_video;
             b_playlist.tag := 0;
             s_left.visible := false;
             s_main.visible := false;
             s_playlist.visible := true;
             spl1.visible := true;
             end;
          end;
  if b_fullscreen.tag = 1
  then begin 
       autocorrect_middle;
       showpanels;
       end;          
  on_resizevideo(sender);
  on_showdubble(sender);
  try
  s_video.setfocus;
  except
  end;  
end;
// ======================================================= 
//         LEFT PANEL
// ======================================================= 
procedure tmainfo.on_dobblevideo(const sender: TObject);
begin
 //if s_video.width <> s_left.width then exit;


 case b_dubble.tag of 
 0:begin
   s_left.width  := s_left.width * 2;
   b_dubble.tag := 1;
   if sender = b_dubble then setsfo.b_dubblevideowimdow.value := true;
   end;
 1:begin
   s_left.width  := s_left.width div 2;
   b_dubble.tag := 0;
   setsfo.b_dubblevideowimdow.value := false;
   end;
 end;//case
 
 s_video.width := s_left.width;
 s_video.height := s_left.width div 2;
 s_left.height := s_middle.height - s_video.height;
 sb_left.left := (s_left.width - sb_left.width) div 2;
 if spl1.anchors = [an_right] 
    then spl1.left := s_left.width + (width - s_left.width) div 2
    else spl1.left := s_left.width;
 spl1.update;
 spl1_left := spl1.left / width;
 on_resizevideo(sender);
 on_showdubble(sender);
end;

//navigate resources
procedure tmainfo.on_navigate_media(const sender: TObject);
var i,y : integer;
begin
  if (sender.classname = 'tmenuitem')
     then i := (sender as tmenuitem).tag
     else i := (sender as trichbutton).tag;
  case i of
  0..5 : begin
         tw_playlist.activepageindex := 0;
         b_repeat.parentwidget := s_pl_top;
         b_forward.parentwidget := s_pl_top;
         end;
  6 :    tw_playlist.activepageindex := 3; //webcamera
  8 :    tw_playlist.activepageindex := 1;
  7 :    tw_playlist.activepageindex := 2; //dvd
  9 :    begin
         tw_playlist.activepageindex := 4; //audio cd
         b_repeat.parentwidget := s_audio_top;
         b_forward.parentwidget := s_audio_top;
         end;
  else tw_playlist.activepageindex := 5;
  end;

  case i of
  6 : begin 
      s_bottom_pos.visible := false;
      s_bottom_right.visible := false;
      end;
  8 : begin 
      s_bottom_pos.visible := false;
      s_bottom_right.visible := true;
      end;
  else 
      begin
      s_bottom_pos.visible := true;
      s_bottom_right.visible := true;
      end;
  end; //case

  case i of
  6,8 : begin 
      b_prev.visible := false;
      b_next.visible := false;
      b_repeat.visible := false;
      end;
  else 
      begin
      b_prev.visible := true;
      b_next.visible := true;
      b_repeat.visible := true;
      end;
  end; //case

  case i of
  7 : begin
      s_btn_player.visible := false;
      s_btn_dvd.visible := true;
      end;
  else 
      begin
      s_btn_player.visible := true;
      s_btn_dvd.visible := false;
      end;
  end; //case

  case i of
  6,7,8,9 : begin
      spl1.anchors := [an_left];
      s_main.bounds_cxmax := 1;
      s_main.bounds_cxmin := 1;
      spl1.left := s_left.width;
      end;
  else 
      begin
      spl1.anchors := [an_right];
      s_main.bounds_cxmax := 0;
      s_main.bounds_cxmin := 0;
      if s_middle.parentwidget = mainfo.container
      then begin 
           spl1.left := round(spl1_left * width);
           end
      else begin
           spl1.left := round(spl1_left * playlistfo.width); 
           end;
      end;
  end; //case
  
  for y := 0 to 9 do 
   begin 
     (findcomponent('bl' + inttostr(y)) as trichbutton).frame.template := frame_buttons;
     if i = y then (findcomponent('bl' + inttostr(y)) as trichbutton).frame.template := frame_edits;
   end;
   tw_main.activepageindex := i;
end;
//collapce / expand
procedure tmainfo.on_navigate2(const sender: TObject);
var s : string;
begin
  s := (sender as trichbutton).name;
  delete(s,1,4);
  if (findcomponent('s_l_' + s) as tspacer).height = 5
    then (findcomponent('s_l_' + s) as tspacer).height := (sender as trichbutton).tag
    else (findcomponent('s_l_' + s) as tspacer).height := 5;
end;

procedure tmainfo.on_show_settings(const sender: TObject);
begin
  if not __showsetsfofirst then 
    begin
      __showsetsfofirst := true;
      setsfo.left := mainfo.left + (mainfo.width - setsfo.width) div 2;
      setsfo.top  := mainfo.top + (mainfo.height - setsfo.height) div 2;
    end; 
  //setsfo.bringtofront; //this first ...
  //setsfo.show; //...show second  bug in linux (openbox ???)
  dubblefo.visible := false;
  set2fo.bringtofront;
  if setsfo.visible then 
    begin 
      setsfo.visible := false;
      application.processmessages;
      mainfo.activate;//setfocus; //not work !!!
      end
  else begin
       //loadsettings;
       setsfo.bringtofront; 
       setsfo.show;//(true);
       end;
end;

// ======================================================= 
//                settings
// ======================================================= 
procedure tmainfo.loadsettings;
var  s : msestring;
    fi : tinifile;
     b : boolean;
    fs : tsearchrec;
  {$ifdef windows}i : integer;{$endif}    
begin
  with setsfo do begin
  s := '128';  dd_cache.dropdown.cols.addrow(s);
  s := '256';  dd_cache.dropdown.cols.addrow(s);
  s := '512';  dd_cache.dropdown.cols.addrow(s);
  s := '1024'; dd_cache.dropdown.cols.addrow(s);
  s := '2048'; dd_cache.dropdown.cols.addrow(s);
  s := '4096'; dd_cache.dropdown.cols.addrow(s);
  s := '6144'; dd_cache.dropdown.cols.addrow(s);
  end;

  fi := tinifile.create(conffile);
  
  //xelplayer
  {$ifdef linux}  
  setsfo.be_showonlymediafiles.value := fi.readbool('xelplayer','showonlymediafiles', true);
  setsfo.fne_magic.value := fi.readstring('xelplayer','MagicFile', '/usr/share/file/misc/magic.mgc');
  if not fileexists(filemagic) then setsfo.be_showonlymediafiles.value := false;
  {$endif} 
  {$ifdef windows}
  setsfo.be_showonlymediafiles.value := false;
  setsfo.be_showonlymediafiles.enabled := false;
  setsfo.fne_magic.enabled := false;
  {$endif}    

  S := dirtowin(fi.readstring('xelplayer', 'FileNavigationFromDirectory', ''));
  if (s = '')or(NOT DIRECTORYEXISTS(s)) then s := apppath;
  
  setsfo.dd_finav_home_dir.VALUE := s;
  DD_DIR.VALUE := s;


  mainfo.wg_fl.visible := setsfo.be_showonlymediafiles.value;
  mainfo.flv.visible := not setsfo.be_showonlymediafiles.value;
    
  if setsfo.be_showonlymediafiles.value 
  then readlist(s)
  else begin
       flv.directory := s;
       flv.readlist;  
       end;
  
  s := dirtowin(fi.readstring('xelplayer', 'PlaylistsDirectory', ''));
  if (s = '')or(NOT DIRECTORYEXISTS(s)) then s := playlistspath;
  setsfo.dd_playlistsdir.VALUE := s;

  setsfo.b_dubblevideowimdow.value := fi.readbool('xelplayer', 'DubbleVideoWindow', false);
  if setsfo.b_dubblevideowimdow.value then 
    begin
      b_dubble.tag := 0;
      on_dobblevideo(setsfo.b_dubblevideowimdow);
    end;

  S := dirtowin(fi.readstring('xelplayer', 'ScreenshotsDirectory', ''));
  if (s = '')or(NOT DIRECTORYEXISTS(s)) then s := apppath;
  setsfo.dd_screenshotpath.value := s;
  setcurrentdir(s);
  
  setsfo.dd_sheme.value := fi.readstring('xelplayer', 'sheme', 'default');
  setsfo.on_load_sheme(setsfo.trichbutton7);

  setsfo.dd_icons.value := fi.readstring('xelplayer', 'icons', 'default');
  setsfo.on_load_icons(setsfo.trichbutton7);

  setsfo.be_nexttrackallow.value := fi.readbool('xelplayer', 'NextTrackAllow', b);
  player.PlayNextTrackAllow := setsfo.be_nexttrackallow.value;
  
  setsfo.be_ask_forexit.value := fi.readbool('xelplayer', 'AskForExit', true);
    
  setsfo.be_showplayling_debug.value := fi.readbool('xelplayer', 'ShowPlayling', true);  
  player.debug_showplaying := setsfo.be_showplayling_debug.value;

  setsfo.be_writetoconsole.value := fi.readbool('xelplayer', 'WriteToConsole', true);  
  player.debug_writeouttoconsole := setsfo.be_writetoconsole.value;
  
  setsfo.be_writetoconsolewithmplayeroutput.value := fi.readbool('xelplayer', 'WriteToConsoleWithMplayerOutput', true); 
  player.debug_AddMplayerOutputToDebug := setsfo.be_writetoconsolewithmplayeroutput.value;

  setsfo.ber_on_desktop.value := fi.readbool('xelplayer', 'VideoToOtherWidget', false);
  setsfo.se_videoID.value := fi.readstring('xelplayer', 'OtherDesktopID', '0'); 
  player.videotoanotherdesktop :=  setsfo.ber_on_desktop.value;
  player.device_CastomDisplayID := setsfo.se_videoID.value;

  setsfo.be_allowloademblems.value := fi.readbool('xelplayer', 'AllowLoadRadioAndTVEmblems', true); 
  if not setsfo.be_allowloademblems.value 
     then setsfo.on_setallowemblems_change(setsfo.be_allowloademblems); //here first load tv and radio chanels 

  setsfo.be_useonecopy.value := fi.readbool('xelplayer','useonecopy', true);
  IF NOT setsfo.be_useonecopy.value THEN b_useone := TRUE else b_useone := false; //IMPORTANT
  
  b_repeat.tag := fi.readinteger('xelplayer','repeatplaylist', 0);
  b_forward.tag := fi.readinteger('xelplayer','forwardplaylist', 0);

  b_windowstyle.tag := fi.readinteger('xelplayer','style', 0);
  if b_windowstyle.tag = 1 then 
    begin
      b_windowstyle.tag := 0;
      on_changewindowstyle(b_windowstyle);
    end;
  //mplayer 
  s := fi.readstring('mplayer', 'Path', '');
  setsfo.fne_mplayer.value := s;
  setsfo.on_set_mplayer(setsfo.fne_mplayer,s,b);

  setsfo.dd_cache.value := fi.readstring('mplayer', 'Cache', '');
  player.cache := setsfo.dd_cache.value;

  setsfo.be_usefifo.value := fi.readbool('mplayer',   'usefifo', false);
  player.usefifo := setsfo.be_usefifo.value;
  
  setsfo.dd_ao.value := fi.readstring('mplayer', 'AudioOutput', 'default');
  setsfo.dd_vo.value := fi.readstring('mplayer', 'VideoOutput', 'default');
  setsfo.be_aval_ao.value := fi.readbool('mplayer', 'ShowAvalAudioVideoOutputOnly', false);
  s := setsfo.dd_vo.value; setsfo.on_setvideooutput(setsfo.dd_vo, s, b);
  s := setsfo.dd_ao.value; setsfo.on_setaudioOutput(setsfo.dd_ao, s, b);
  b := setsfo.be_aval_ao.value;  setsfo.on_aval_audio_only(setsfo.be_aval_ao,b,b);
  
  setsfo.se_webcam_preload.value := fi.readstring('mplayer', 'WebcamPreload', '');
  setsfo.se_additional_video_params.value := fi.readstring('mplayer', 'AditionalVideoParams', '');
  setsfo.se_additional_params.value := fi.readstring('mplayer', 'AditionalParams', '');
  setsfo.be_indexing.value := fi.readbool('mplayer', 'indexing', false);
  player.indexing := setsfo.be_indexing.value;
  setsfo.be_doublebuf.value := fi.readbool('mplayer', 'doublebuffering', false);
  player.doublebuf := setsfo.be_doublebuf.value;

  setsfo.be_noscreensaver.value := fi.readbool('mplayer', 'disablescreensaver', true);
  player.disablescreensaver := setsfo.be_noscreensaver.value;
  
  player.volume := fi.readinteger('xelplayer','volume', 50);
  //devices
  setsfo.ddd_webcam.value := fi.readstring('Devices', 'Webcamera', '');
  ddd_webcam.value := fi.readstring('Devices', 'Webcamera', '');
  setsfo.ddd_tvtuner.value := fi.readstring('Devices', 'TVTuner', '');
  setsfo.ddd_audioCDdevice.value := fi.readstring('Devices', 'CDROM', '');
  ddd_audioCDdevice.value := fi.readstring('Devices', 'CDROM', '');
  ddd_audioCDdevice2.value := fi.readstring('Devices', 'CDROM', '');
  player.device_webcamera := setsfo.ddd_webcam.value;
  player.device_tvtuner := setsfo.ddd_tvtuner.value;
  player.device_cdrom := setsfo.ddd_audioCDdevice.value;
  setsfo.on_update_devices(setsfo.b_update_devices);

  loadfo.l_.caption := 'Set sheme ...'; loadfo.update; 
  //editshemefo.on_apply(setsfo);
  setsfo.on_load_sheme(setsfo.trichbutton7);

  if b_repeat.tag = 1 then //correct after load sheme
   begin
       player.repeatplaylist := TRUE; 
       b_repeat.imagenr := 27;
   end;   

  if b_forward.tag = 1 then //correct after load sheme
   begin
       player.playplaylistback := TRUE;
       b_forward.imagenr := 26;
   end;   

  //langs
  loadfo.l_.caption := 'Set language ...'; loadfo.update; 
  setsfo.dd_langs.dropdown.cols.clear;
  if findfirst(langspath + '*.lang',faanyfile,fs) = 0 then
  repeat 
    setsfo.dd_langs.dropdown.cols.addrow(msestring(fs.name));
  until findnext(fs) <> 0;
  findclose(fs); 
  setsfo.dd_langs.value := fi.readstring('xelplayer', 'language', 'en.lang');
  //setsfo.on_set_lang(setsfo.trichbutton5);

  fi.free;
  if not fileexists(conffile) then 
     show_ask(mainfo,'Configuration file not found!' + #10 + 
              'Use defaults settings.' + #10 + 
              'This normally if xelplayer is runned first time',
              '',false,false,false);  
end;

procedure tmainfo.savesettings;
var fi : tinifile;
begin
  TRY  // PREVENT IF SETTINGS FILE COUND NOT BE CREATE
  fi := tinifile.create(conffile);

  fi.writestring('xelplayer', 'FileNavigationFromDirectory', setsfo.dd_finav_home_dir.VALUE);
  fi.writestring('xelplayer', 'PlaylistsDirectory', setsfo.dd_playlistsdir.VALUE);

  fi.writebool('xelplayer', 'DubbleVideoWindow', setsfo.b_dubblevideowimdow.value);
  fi.writestring('xelplayer', 'ScreenshotsDirectory', setsfo.dd_screenshotpath.value);
  fi.writebool('xelplayer',   'NextTrackAllow', setsfo.be_nexttrackallow.value);
  fi.writebool('xelplayer',   'AskForExit', setsfo.be_ask_forexit.value );

  fi.writebool('xelplayer',   'WriteToConsole',setsfo.be_writetoconsole.value);  
  fi.writebool('xelplayer',   'WriteToConsoleWithMplayerOutput',  setsfo.be_writetoconsolewithmplayeroutput.value); 
  fi.writebool('xelplayer', 'ShowPlayling', setsfo.be_showplayling_debug.value);  

  fi.writebool('xelplayer',   'VideoToOtherWidget', setsfo.ber_on_desktop.value);
  fi.writestring('xelplayer', 'OtherDesktopID', setsfo.se_videoID.value); 

  fi.writestring('xelplayer', 'sheme', setsfo.dd_sheme.value); 
  fi.writestring('xelplayer', 'icons', setsfo.dd_icons.value); 
  fi.writestring('xelplayer', 'language', setsfo.dd_langs.value);

  fi.writebool('xelplayer', 'AllowLoadRadioAndTVEmblems', setsfo.be_allowloademblems.value); 

  fi.writebool('xelplayer','useonecopy', setsfo.be_useonecopy.value);

  fi.writebool('xelplayer','showonlymediafiles', setsfo.be_showonlymediafiles.value);

  fi.writestring('xelplayer','MagicFile', setsfo.fne_magic.value);
  
  fi.writeinteger('xelplayer','repeatplaylist', b_repeat.tag);
  fi.writeinteger('xelplayer','forwardplaylist', b_forward.tag);
  
  fi.writeinteger('xelplayer','style', b_windowstyle.tag);
  //mplayer 
  fi.writestring('mplayer', 'Path', setsfo.fne_mplayer.value);

  fi.writestring('mplayer', 'Cache', setsfo.dd_cache.value);

  fi.writestring('mplayer', 'AudioOutput', setsfo.dd_ao.value);
  fi.writestring('mplayer', 'VideoOutput', setsfo.dd_vo.value);
  fi.writebool('mplayer',   'ShowAvalAudioVideoOutputOnly', setsfo.be_aval_ao.value);
  fi.writebool('mplayer',   'usefifo', setsfo.be_usefifo.value);
  fi.writestring('mplayer', 'WebcamPreload', setsfo.se_webcam_preload.value);
  fi.writestring('mplayer', 'AditionalVideoParams', setsfo.se_additional_video_params.value);
  fi.writestring('mplayer', 'AditionalParams', setsfo.se_additional_params.value);
  fi.writebool('mplayer',   'Indexing', setsfo.be_indexing.value);
  fi.writebool('mplayer', 'doublebuffering', setsfo.be_doublebuf.value);
  fi.writebool('mplayer', 'disablescreensaver', setsfo.be_noscreensaver.value);
  fi.writeinteger('xelplayer','volume', player.volume);

  //devices
  fi.writestring('Devices', 'Webcamera', setsfo.ddd_webcam.value);
  fi.writestring('Devices', 'TVTuner', setsfo.ddd_tvtuner.value);
  fi.writestring('Devices', 'CDROM', setsfo.ddd_audioCDdevice.value);  

  fi.free;
  EXCEPT
  show_ask(mainfo,'Can`t create settings file!' + #10 + 
           '(' + conffile + ')','',false,false,false);
  END;
end;

// ======================================================= 
//                MIDDLE
// ======================================================= 
procedure tmainfo.on_hide_addons(const sender: TObject);
begin
  spl2.top := s_middle.height - spl2.height;
end;



// ======================================================= 
//                FILE OPERATIONS
// ======================================================= 
function tmainfo.show_ask(const sender : tobject; capt,fname : string; ddd_visible, se_visible,yes_visible : boolean) : modalresultty;
begin
  askfo.left := (sender as tcustommseform).left + ((sender as tcustommseform).width - askfo.width) div 2;
  askfo.top  := (sender as tcustommseform).top + ((sender as tcustommseform).height - askfo.height) div 2; 
         
  askfo.l_.caption := capt;
  askfo.SE.value := fname;
  askfo.ddd.visible := ddd_visible; 
  askfo.se.visible := se_visible; 
  askfo.b_yes.visible := yes_visible;
  askfo.bringtofront;
  result := askfo.show(true);
end;

function tmainfo.get_mainwindow : tcustommseform;
begin
  if b_windowstyle.tag = 0
    then result := mainfo
    else result := playlistfo;
end;

procedure tmainfo.rename_file(fold : string; updateplaylist, update_flv : boolean);
var i : integer;
begin
  if show_ask(get_mainwindow,'Rename?',extractfilename(fold),false,true,true) = mr_ok then 
    if not msefileutils.renamefile(fold, extractfilepath(fold) + '/' + askfo.SE.value)
       then show_ask(get_mainwindow,'Can`t rename'+#10+'(newname exists and not canoverwrite)!','',false,false,false)
       else begin
            if updateplaylist 
               then begin
                    if wg_playlist.rowcount > 0 then
                      for i := 0 to wg_playlist.rowhigh do
                       if se_url[i] = fold then
                        begin
                        se_url[i] := extractfilepath(fold) + '/' + askfo.SE.value;
                        se_name[i] := askfo.SE.value;
                        end;
                    end;
            if update_flv 
               then BEGIN
                    if not setsfo.be_showonlymediafiles.value then
                    for i:= 0 to flv.filelist.count - 1 do 
                      if flv.itemlist[i].caption = extractfilename(fold) then
                       begin
                        flv.readlist;
                        break;
                       end;
                    if setsfo.be_showonlymediafiles.value then
                    for i := 0 to wg_fl.rowhigh do
                    if se_fname[i] = extractfilename(fold) then 
                      begin
                        readlist(dd_dir.value);
                        break;
                      end;
                    END; 
            end;
end;

Function tmainfo.MyRemoveDir(sDir : String) : Boolean; //http://samouchka.net/2007/02/19/kopirovanie_i_udalenie_fajjlov_v_delphi.html with my changes
var 
 iIndex : Integer; 
 SearchRec : TSearchRec; 
 sFileName : String;
 c : msechar; 
begin 
 Result := False; 
 {$ifdef mswindows}
 c := '\';
 if sdir[1] = '/' then delete(sdir,1,1);
 for iindex := 1 to length(sdir) do 
   if sdir[iindex] = '/' then sdir[iindex] := '\';
 {$endif}
 {$ifdef linux}c := '/';{$endif} 
   
 sDir := sDir + c + '*'; 
 iIndex := FindFirst(sDir, faAnyFile, SearchRec); 

 while iIndex = 0 do begin 
 sFileName := ExtractFileDir(sDir) + c + SearchRec.Name; 
 if ((faDirectory and SearchRec.Attr) <> 0) then 
 begin 
  if (SearchRec.Name <> '' ) and (SearchRec.Name <> '.') and 
     (SearchRec.Name <> '..') then MyRemoveDir(sFileName); 
 end 
 else 
 begin 
  {$ifdef mswindows}
  if (faArchive AND SearchRec.Attr) <> 0 then FileSetAttr(sFileName, faArchive); 
  {$endif} 
  if (SearchRec.Name <> '' ) and (SearchRec.Name <> '.') and (SearchRec.Name <> '..') then  
      if NOT tryDeleteFile(sFileName) then ShowMessage('Could NOT delete ' + sFileName); 
 end; 
 iIndex := FindNext(SearchRec); 
 end; 

 FindClose(SearchRec); 
 {$ifdef mswindows}
 RemoveDir(msestring(ExtractFileDir(sDir))); /// <- not work with cyrrilic !!!!
 {$endif} 
 {$ifdef inux}
 RemoveDir(ExtractFileDir(sDir)); 
 {$endif} 
 Result := True; 
end;
 
procedure tmainfo.copyfiles(sl, sl2 : tstringlist; copy_ : boolean);
var i : integer;
begin
  if sl.count > 0 then 
  begin
    copyfo.left := mainfo.left + (mainfo.width - copyfo.width) div 2;
    copyfo.top  := mainfo.top + (mainfo.height - copyfo.height) div 2; 
    copyfo.l_.caption := '0 / ' + inttostr(sl.count);
    copyfo.pb.value := 0;
    copyfo.bringtofront;
    copyfo.show;
    application.processmessages;
    for i := 0 to sl.count - 1 do
      begin
        if copy_
           then copyfile(sl[i], sl2[i])
           else renamefile(sl[i], sl2[i]);
        copyfo.l_.caption := inttostr(i + 1) + ' / ' + inttostr(sl.count);
        copyfo.pb.value   := (i + 1) / sl.count;
        application.processmessages;
      end;
    show_ask(get_mainwindow,'Done','',false,false,false);
    copyfo.close;
  end;
end;
// ======================================================= 
//              ADD TO PLAYLIST
// ======================================================= 
//file
procedure tmainfo.addfile(fcapt, url, emblem :string);
var r : integer;
begin 
  {$ifdef mswindows} if url[1] = '/' then delete(url,1,1);{$endif}
  r := wg_playlist.rowcount;
  wg_playlist.rowcount := r + 1;
  se_url[r] := url;
  se_name[r] := fcapt;
  se_emblem[r] := emblem;
  player.playlist.add(url);
end;
//dir
procedure tmainfo.adddir(path : string);
var fs : tsearchRec;
begin
{$ifdef mswindows} if path > '' then if path[1] = '/' then delete(path,1,1);{$endif}
    if findfirst(path + '/*', faanyfile, fs) = 0 then
    repeat
     if (fs.name = '')or(fs.name = '.')or(fs.name = '..') then continue;
     if fs.Attr and faDirectory <> 0 
       then adddir(path + '/' + fs.name)
       else addfile(fs.name, path + '/' + fs.name, '');    
    until findnext(fs) <> 0;
    findclose(fs);
end; 
// ======================================================= 
//               FILE NAVIGATOR
// ======================================================= 
procedure tmainfo.on_set_directory_finav(const sender: TObject; var avalue: msestring; var accept: Boolean);
begin
  if setsfo.be_showonlymediafiles.value
  then readlist(avalue)
  else begin
       flv.directory := avalue;
       flv.readlist; 
       end;
end;

procedure tmainfo.on_go_home(const sender: TObject);
begin
  dd_dir.value := setsfo.dd_finav_home_dir.value;
  if setsfo.be_showonlymediafiles.value
  then  readlist(setsfo.dd_finav_home_dir.value)
  else begin
       flv.directory := setsfo.dd_finav_home_dir.value;
       flv.readlist;  
       end;
end;

procedure tmainfo.on_go_up(const sender: TObject);
var s : string;
begin
  if not setsfo.be_showonlymediafiles.value
  then begin
       flv.updir;
       dd_dir.value := flv.directory;
       exit;
       end;
  s := dd_dir.value;
  if s[length(s)] = '/' then delete(s,length(s),1);
  if s = '' then exit;
  s := extractfilepath(s);
  dd_dir.value := s;
  readlist(s);
end;

procedure tmainfo.on_rename_file_in_flv(const sender: TObject);
begin
  if not setsfo.be_showonlymediafiles.value
  then begin
       if flv.focusedindex < 0 then exit;
       rename_file(flv.directory + flv.items[flv.focusedindex].caption, true, true); 
       exit;
       end;
  if wg_fl.row < 0 then exit;
  rename_file(se_furl[wg_fl.row], true, true); 
end;

procedure tmainfo.on_deletefile_flv(const sender: TObject);
var a : array of msestring;
    i : integer;
    s : string;
begin
 {$ifdef mswindows}
 showmessage('FIX ME: Write cyrilic names delete ERROR!');
 {$endif} 
  if not setsfo.be_showonlymediafiles.value
  then begin
       if flv.focusedindex < 0 then exit;
       if show_ask(get_mainwindow,'Delete file(s)?','',false,false,true) <> mr_ok then exit;
       a := flv.selectednames;
       s := flv.directory;
       for i := high(a) downto 0 do 
         if flv.filelist.isdir(flv.filelist.indexof(a[i]))
            then Myremovedir(s + a[i])
            else trydeletefile(s + a[i]);
       flv.readlist;
       exit;
       end;

  if wg_fl.rowcount = 0 then exit;
  if wg_fl.row < 0 then exit; 
  if show_ask(get_mainwindow,'Delete file(s)?','',false,false,true) <> mr_ok then exit;
  for i := wg_fl.rowcount downto 0 do 
    begin
      if extractfilename(se_furl[wg_fl.row]) <> se_fname[wg_fl.row] 
      then Myremovedir(se_furl[i])
      else trydeletefile(se_furl[i]);
    end;
  readlist(dd_dir.value);
  show_ask(get_mainwindow,'Done','',false,false,false);
end;

procedure tmainfo.on_reread_flv(const sender: TObject);
begin
  if setsfo.be_showonlymediafiles.value
  then readlist(dd_dir.value)
  else flv.readlist;  
end;

procedure tmainfo.on_filenavigate(const sender: tcustomlistview;const index: Integer; var info: celleventinfoty);
begin
 if iscellclick(info,[ccr_dblclick]) then 
   if flv.filelist.isdir(flv.focusedindex) 
      then begin
            flv.directory := flv.directory + flv.items[flv.focusedindex].caption;
            flv.readlist;  
            dd_dir.value := flv.directory;
           end
      else on_addfilefromdisk(b_open_from_flv);
end;

procedure tmainfo.on_addfilefromdisk(const sender: TObject);
var i, t : integer;
    ar : integerarty;
begin
  if setsfo.be_showonlymediafiles.value
  then begin
       if wg_fl.row < 0 then exit;
       end
  else if flv.focusedindex < 0 then exit;

  if sender.classname = 'trichbutton'
     then t := (sender as trichbutton).tag
     else t := (sender as tmenuitem).tag;
  if t = 0 then
    begin
     on_stop(sender);
     on_clearplaylist(sender);
    end;
  
  if setsfo.be_showonlymediafiles.value then 
  if wg_fl.rowcount > 0 then
  begin
    ar := wg_fl.datacols.selectedrows;
    for i := 0 to high(ar) do 
     if extractfilename(se_furl[ar[i]]) <> se_fname[ar[i]] 
       then adddir(se_furl[ar[i]])
       else addfile(se_fname[ar[i]], se_furl[ar[i]], '');
  end;
  
  if not setsfo.be_showonlymediafiles.value then
  for i:= 0 to flv.filelist.count - 1 do 
    if flv.itemlist[i].selected then
     begin
       if flv.filelist.isdir(i)
         then adddir(flv.path + flv.itemlist[i].caption)
         else addfile(flv.itemlist[i].caption, flv.path + flv.itemlist[i].caption, '');
     end;
  if t = 0 then on_play(sender);
end;

// ======================================================= 
//               LAST TRACKS
// ======================================================= 
procedure tmainfo.on_add_from_lastplayed(const sender: TObject);
var i,t : integer;
    ar: integerarty;
begin
  if wg_last.row < 0 then exit;
  if sender.classname = 'trichbutton'
     then t := (sender as trichbutton).tag
     else t := (sender as tmenuitem).tag;
       
  if t = 0 then on_clearplaylist(sender);
  
  ar := wg_last.datacols.selectedrows;
  for i := 0 to high(ar) do
    addfile(se_name_last[ar[i]], se_url_last[ar[i]], se_emblem_last[ar[i]]);

  if t = 0 then 
    begin
      on_stop(sender);
      on_play(sender);
    end; 
end;
//DBLCLICK EVENT
procedure tmainfo.on_add_file_from_last_tracks(const sender: TObject;var info: celleventinfoty);
begin
  IF wg_last.ROW < 0 THEN EXIT;
  if iscellclick(info,[ccr_dblclick])  then on_add_from_lastplayed(trichbutton11);  
end;

procedure tmainfo.on_clear_list_of_last_played(const sender: TObject);
begin
  if show_ask(get_mainwindow,'Clear ?','', false,false,true) <> mr_ok then exit;
  if trydeletefile(dirtowin(setsfo.dd_playlistsdir.value) + '__LAST_PLAYED__')
    then begin
         wg_last.clear;
         end
    else show_ask(get_mainwindow,'Can`t delete ' + dirtowin(setsfo.dd_playlistsdir.value) + '__LAST_PLAYED__','', false,false,true);
end;

procedure tmainfo.on_delete_from_last_played(const sender: TObject);
var i : integer;
    f : textfile;
    ar: integerarty;
begin
  if wg_last.row < 0 then exit;
  if show_ask(get_mainwindow,'Errase records(s) ?','', false,false,true) <> mr_ok then exit;  
  ar := wg_last.datacols.selectedrows;
  for i := high(ar) downto 0 do wg_last.deleterow(ar[i]);
  if wg_last.rowhigh >= 0 then 
  begin
  assignfile(f,dirtowin(setsfo.dd_playlistsdir.value) + '__LAST_PLAYED__');
  rewrite(f);
  for i := 0 to wg_last.rowhigh do
    begin
      writeln(f,se_name_last[i]);
      writeln(f,se_url_last[i]);
      writeln(f,se_emblem_last[i]);
    end;
  closefile(f);
  end;
end;

// ======================================================= 
//               MY PLAYLISTS
// ======================================================= 
procedure tmainfo.on_loadplaylists_list(const sender: TObject);
var fs : tsearchrec;
    dir: string;
    sl : tstringlist;

begin
  wg_myplaylists.clear;  
  sl := tstringlist.create;
  dir := dirtowin(SETSFO.dd_playlistsdir.value);
  if findfirst(dir + '*', faanyfile, fs) = 0 then
  repeat
    if (fs.name = '.')or(fs.name = '..')or(fs.name = '') then continue;
    try // prevent if unsuported simbols in file names - error in linux
      if fs.name = '__LAST_PLAYED__' then 
        begin
         loadlast(dir);
         continue;
        end;
      //cheching if file really PLS playlist
      sl.loadfromfile(dir + fs.name);    //      assignfile(f, dir + fs.name);  not work in windows (with utf8 ???)
      if trim(sl[0]) <> '[playlist]' then continue;
      wg_myplaylists.rowcount := wg_myplaylists.rowcount + 1;
      se_myplaylist[wg_myplaylists.rowcount - 1] := fs.name;
    except 
      wg_myplaylists.deleterow(wg_myplaylists.rowhigh);
    end;
  until findnext(fs) <> 0;
  sl.free;
  wg_myplaylists.sort;
  findclose(fs);
end;

procedure tmainfo.save_in_my_playlists(const wg : twidgetgrid; const se: tstringedit; fname : string);
var  i : integer;
    sl : tstringlist;
begin
  sl := tstringlist.create;
  sl.add('[playlist]');
  sl.add('NumberOfEntries='+inttostr(wg.rowcount));
  if wg.rowcount > 0 then 
  for i := 0 to wg.rowhigh do sl.add('File'+ inttostr(i) + '=' + se[i]);
  sl.savetofile(fname);
  sl.free;
  on_loadplaylists_list(b_reloadplaylists);
end;  

procedure tmainfo.on_rename_my_playlist(const sender: TObject);
begin
  if wg_myplaylists.row < 0 then exit;
  rename_file(dirtowin(setsfo.dd_playlistsdir.value) + se_myplaylist[wg_myplaylists.row], false, false);
  on_loadplaylists_list(sender);
end;

procedure tmainfo.on_delete_from_my_playlists(const sender: TObject);
var i : integer;
    ar: integerarty;
    b : boolean;
begin
  if wg_myplaylists.row < 0 then exit;
  if show_ask(get_mainwindow,'Errase playlists(s) ?','', false,false,true) <> mr_ok then exit;  
  b := false;
  ar := wg_myplaylists.datacols.selectedrows;
  for i := high(ar) downto 0 do 
  if deletefile(dirtowin(setsfo.dd_playlistsdir.value) + se_myplaylist[ar[i]]) 
     then wg_myplaylists.deleterow(ar[i])
     else b := true;
  if b then show_ask(get_mainwindow,'Not all selected playlists were deleted. Check rights!','',false,false,false); 
end;

procedure tmainfo.on_add_myplaylist_To_Playlist(const sender: TObject);
var sl : tstringlist;
   y,i, t : integer;
    ar : integerarty;
begin
  if wg_myplaylists.row < 0 then exit;
  
  if sender.classname = 'trichbutton'
     then t := (sender as trichbutton).tag
     else t := (sender as tmenuitem).tag;
     
  if t = 0 then
    begin
      on_stop(sender);
      on_clearplaylist(sender);
    end;
    
  ar := wg_myplaylists.datacols.selectedrows;
  for i := 0 to high(ar) do 
  begin
    sl:= tstringlist.create;
    openpls_playlist(dirtowin(setsfo.dd_playlistsdir.value) + se_myplaylist[ar[i]], sl);
    for y := 0 to sl.count - 1 do addfile(extractfilename(sl[y]), sl[y], '');
    sl.free;
 end;
  if t = 0 then on_play(sender);
end;

procedure tmainfo.on_dblclick_myplaylists(const sender: TObject;var info: celleventinfoty);
begin
 if iscellclick(info,[ccr_dblclick]) then on_add_myplaylist_To_Playlist(b_open_from_myplaylists);
end;

procedure tmainfo.on_save_in_my_playlists(const sender: TObject);
begin
 if wg_playlist.rowcount = 0 then exit;
 if show_ask(get_mainwindow,'Save in my playlists?', '', false,true,true) = mr_ok
    then save_in_my_playlists(wg_playlist, se_url, dirtowin(setsfo.dd_playlistsdir.value) + askfo.SE.text);
end;

procedure tmainfo.on_rewrite_my_playlist(const sender: TObject);
begin
  if wg_myplaylists.row < 0 then exit;
  if wg_playlist.rowcount = 0 then exit;  
  if show_ask(get_mainwindow,'Rewrite this playlist?', '', false,false,true) <> mr_ok then exit;
  save_in_my_playlists(wg_playlist,se_url, dirtowin(setsfo.dd_playlistsdir.value) + se_myplaylist[wg_myplaylists.row]);
end;

// ======================================================= 
//               RADIO AND TV ONLINE CHANELS
// ======================================================= 
procedure tmainfo.on_LOADRADIOtv(const sender: TObject);
var t : integer;
begin
  if sender.classname = 'trichbutton' 
  then t := (sender as trichbutton).tag
  else t := tw_main.activepageindex - 4;
  
  if t = 0 
  then begin
       loadradiotv(radiotvpath + 'radio/radio.conf', radiotvpath + 'radio/images/',wg_radio);
       wg_radio.sort;
       end
  else begin
       loadradiotv(radiotvpath + 'tv/tv.conf', radiotvpath + 'tv/images/', wg_tv);
       wg_tv.sort;
       end;
end;
//MAIN LOADING ONLINE chanels PROCEDURE
procedure tmainfo.loadradiotv(fname, prev : string; wg : twidgetgrid);
const thumbsize: sizety = (cx:60;cy:30);
var  fi : tinifile;
     sl : tstringlist;
    i,r : integer;
   s,s2,s3 : string;
   astream: tstream;//tstringstream;
begin 
 {$ifdef mswindows} if fname > '' then if fname[1] = '/' then delete(fname,1,1);{$endif}
 sl := tstringlist.create;
 fi := tinifile.create(fname);
 fi.readsections(sl);
 wg.clear;
 if wg = wg_radio then s2 := 'radio'
                  else s2 := 'tv';

 if not setsfo.be_allowloademblems.value 
    then wg.datacols[0].width := 0
    else wg.datacols[0].width := 60;
                  
 if sl.count > 0 then
 for i := 0 to sl.count - 1 do
  begin
    r := wg.rowcount;
    wg.rowcount := r + 1;
    s := fi.readstring(sl[i],'emblem','');
    try
    if setsfo.be_allowloademblems.value 
      then if s > '' then 
        if fileexists(prev + s) 
          then 
            begin
              (findcomponent('di_emblem_' + s2) as tdataimage).gridvalue[r] := readfiledatastring(prev + s);
            end;
    except
    end;
    (findcomponent('se_name_' + s2) as tstringedit)[r] := utf8toansi(sl[i]);
    (findcomponent('se_url_' + s2) as tstringedit)[r] := fi.readstring(sl[i],'url','');
    (findcomponent('se_emblem_' + s2) as tstringedit)[r] := prev + s;
    (findcomponent('se_site_' + s2) as tstringedit)[r] := fi.readstring(sl[i],'site','');
  end; 
 fi.free;
end;

procedure tmainfo.on_delete_chanel(const sender: TObject);
var fi : tinifile;
    wg : twidgetgrid;
    se : tstringedit;
     i : integer;
    ar : integerarty;
begin
 if tw_main.activepage = tp_radio 
    then wg := wg_radio
    else wg := wg_tv;

 if wg.row < 0 then exit;
 if show_ask(get_mainwindow,'Errase chanel(s) ?','', false,false,true) <> mr_ok then exit;
 
 if tw_main.activepage = tp_radio 
    then begin se := se_name_radio;fi := tinifile.create(radiotvpath + 'radio/radio.conf');end
    else begin se := se_name_tv;   fi := tinifile.create(radiotvpath + 'tv/tv.conf'); end;

 ar := wg.datacols.selectedrows; 
 for i := high(ar) downto 0 do                
  fi.EraseSection( ansitoutf8(se[ar[i]]) );  //we convert string when open ini, now convert again (issue in windows)

 fi.free;
 if tw_main.activepage = tp_radio 
    then on_loadradiotv(trichbutton20)
    else on_loadradiotv(trichbutton21);
end;

procedure tmainfo.on_add_radio_chanel(const sender: TObject);
begin
  chanelfo.im_emblem.bitmap.clear;
  dubblefo.visible := false;
  chanelfo.se_name.value := '';
  chanelfo.se_url.value := '';
  chanelfo.fne_emblem.value := '';
  chanelfo.se_key.value := ''; //if save chanel we errase prev section
  chanelfo.bringtofront;
  if chanelfo.show(true) = mr_ok then on_save_chanel(sender);
end;

procedure tmainfo.on_edit_chanel(const sender: TObject);
begin
  dubblefo.visible := false;
  chanelfo.im_emblem.bitmap.clear;
  if tw_main.activepage = tp_radio 
    then  begin 
       if wg_radio.row < 0 then exit;
       chanelfo.se_key.value  := se_name_radio[wg_radio.row];
       chanelfo.se_name.value := se_name_radio[wg_radio.row];
       chanelfo.se_url.value  := se_url_radio[wg_radio.row];
       chanelfo.se_site.value := se_site_radio[wg_radio.row];
       chanelfo.fne_emblem.value := se_emblem_radio[wg_radio.row];      
     end
     else begin 
       if wg_tv.row < 0 then exit;
       chanelfo.se_key.value  := se_name_tv[wg_tv.row];
       chanelfo.se_name.value := se_name_tv[wg_tv.row];
       chanelfo.se_url.value  := se_url_tv[wg_tv.row];
       chanelfo.se_site.value := se_site_tv[wg_tv.row];
       chanelfo.fne_emblem.value := se_emblem_tv[wg_tv.row];      
     end;
  try
    chanelfo.im_emblem.bitmap.loadfromfile(chanelfo.fne_emblem.value);
  except
    writeln('error while loading ' + chanelfo.fne_emblem.value);
  end;
  chanelfo.bringtofront;
  if chanelfo.show(true) = mr_ok then on_save_chanel(sender);
end;

procedure tmainfo.on_save_chanel(const sender: TObject);
var fi : tinifile;
     S : STRING;
begin
 if tw_main.activepage = tp_radio then fi := tinifile.create(radiotvpath + 'radio/radio.conf')
                                  else fi := tinifile.create(radiotvpath + 'tv/tv.conf');
 if tw_main.activepage = tp_radio then s := radiotvpath + 'radio/images/'
                                  else s := radiotvpath + 'tv/images/';
                                  
 fi.EraseSection(ansitoutf8(chanelfo.se_key.text)); //because we can change name = key value
 
 
 fi.writestring(ansitoutf8(chanelfo.se_name.value), 'url',    chanelfo.se_url.value);
 fi.writestring(ansitoutf8(chanelfo.se_name.value), 'emblem', extractfilename(chanelfo.fne_emblem.value));
 fi.writestring(ansitoutf8(chanelfo.se_name.value), 'site',   extractfilename(chanelfo.se_site.value));
 fi.free;
 copyfile(chanelfo.fne_emblem.value, s + extractfilename(chanelfo.fne_emblem.value));
 if tw_main.activepage = tp_radio then on_loadradiotv(trichbutton20)
                                  else on_loadradiotv(trichbutton21);
end;

procedure tmainfo.on_add_radioTVchanel_to_playlist(const sender: TObject);
var  i,t : integer;
    wg : twidgetgrid;
    s2 : string;
    ar : integerarty;
begin
  if sender.classname = 'trichbutton'
     then t := (sender as trichbutton).tag
     else t := (sender as tmenuitem).tag;
     
  if t = 0 then
    begin
     on_stop(sender);
     on_clearplaylist(sender);
    end;
     
  if tw_main.activepage = tp_radio 
     then wg := wg_radio
     else wg := wg_tv;
     
  if wg = wg_radio then s2 := 'radio'
                   else s2 := 'tv';
                  
  if wg.row < 0 then exit;
  ar := wg.datacols.selectedrows; 
  for i := 0 to high(ar) do                
     addfile((findcomponent('se_name_' + s2) as tstringedit)[ar[i]], 
             (findcomponent('se_url_' + s2) as tstringedit)[ar[i]], 
             (findcomponent('se_emblem_' + s2) as tstringedit)[ar[i]]);
  
 if t = 0 then on_play(sender);     
end;

procedure tmainfo.on_radioTVlist_cell_event(const sender: TObject; var info: celleventinfoty);
begin
 if iscellclick(info,[ccr_dblclick])then  on_add_radioTVchanel_to_playlist(b_open_from_radiolist);
end;

// ======================================================= 
//               PLAYLIST
// ======================================================= 
//OPEN GROUP
procedure tmainfo.ON_SHOW_PL_POPUP(const sender: TObject);
 var gr : mouseeventinfoty;
begin
  gr.pos.x := 0;
  gr.pos.y := b_menu.height;
  pm_pl.show(b_menu, gr);
end;

procedure tmainfo.findcurrentruck(_restore : boolean);
var i,r : integer;
begin
  r := wg_playlist.row;
  wg_playlist.row := -1;
//  wg_playlist.update;
  if _restore then player.playlist.clear;
  for i := 0 to wg_playlist.rowhigh do 
   begin
     if _restore then player.playlist.add(se_url[i]);
     if se_cur[i] > '' then 
       begin 
         player.tracknum := i; 
         wg_playlist.rowfontstate[i] := 0;
       end
       else
       wg_playlist.rowfontstate[i] := -1;
   end;
   wg_playlist.row := r;
end;

procedure tmainfo.on_addfiles(const sender: TObject);
var s : array of msestring;
    i : integer;
begin
  fd.controller.filter := '';
  if fd.execute = mr_Ok then
  begin
    s := fd.controller.filenames;
    for i := 0 to high(s) do addfile(EXTRACTFILENAME(s[i]), S[I], '');
    if player.tracknum = -1 then player.tracknum := 0;
    if not player.playing then on_play(sender);
  end;
end;

procedure tmainfo.on_add_url(const sender: TObject);
begin
 if show_ask(get_mainwindow,'Add URL','',false,true,true) = mr_ok then addfile(askfo.se.value, askfo.se.value, '');
end;

procedure tmainfo.on_openplaylist(const sender: TObject);
var s : array of msestring;
    i : integer;
   sl, sl2 : tstringlist; 

procedure addfiles;
var y : integer;
begin
   if sl.count > 0 then 
        for y := 0 to sl.count - 1 do 
            addfile(EXTRACTFILENAME(sl[y]), sl[y], '');
end;   

procedure addfiles2;
var y : integer;
begin
   if sl.count > 0 then 
        for y := 0 to sl.count - 1 do 
             addfile(sl2[y], sl[y], '');
end;   
   
begin
 fd.controller.filterlist.clear;
 fd.controller.filterlist.add('Все файлы','*');
 fd.controller.filterlist.add('PLS-плейлисты','*.pls');
 fd.controller.filterlist.add('M3U-плейлисты','*.m3u');
 fd.controller.filterlist.add('KPL-плейлисты','*.kpl');
 fd.controller.filterlist.add('ASX-плейлисты','*.asx');
 fd.controller.filterlist.add('ONLINE-плейлисты','*.online');

 if fd.execute = mr_Ok then
 begin
  s := fd.controller.filenames;
  wg_playlist.clear;
  player.playlist.clear;
  sl := tstringlist.create;
  sl2:= tstringlist.create;
  for i := 0 to high(s) do
   begin
     s[i] := dirtowin(extractfilepath(s[i])) + extractfilename(s[i]);
     sl.loadfromfile(s[i]);
     sl.text := trim(sl.text);
     if (lowercase(extractfileext(s[i])) = '.pls') or ((trim(sl[0]) = '[playlist]')and(system.pos('NumberOfEntries',sl[1]) > 0 )) then 
           begin
             openpls_playlist(s[i], sl);
             addfiles;
           end;
     if (lowercase(extractfileext(s[i])) = '.m3u') or (trim(sl[0]) = '#EXTM3U') then 
           begin
             openm3u_playlist(s[i], sl, sl2);
             addfiles2;
           end;  
     if (lowercase(extractfileext(s[i])) = '.kpl') or ((trim(sl[0]) = '[playlist]')and(system.pos('NumberOfEntries',sl[1]) = 0 ))then 
           begin
             showmessage('fix me in windows');
             openkpl_playlist(s[i], sl);
             addfiles;
           end;
     if (lowercase(extractfileext(s[i])) = '.asx') or (system.pos('<Asx',sl[0]) > 0 ) then 
           begin
             showmessage('fix me');
             //openasx_playlist(s[i], sl, sl2);
             //addfiles;
           end;
     if lowercase(extractfileext(s[i])) = '.online' then
           begin
             openOnline_playlist(s[i], sl2, sl);
             addfiles2;
           end;
   end;
  sl.free; 
  sl2.free; 
 end;
end;

procedure tmainfo.on_open_directory(const sender: TObject);
begin
 if show_ask(get_mainwindow,'Add directory','',true,false,true) = mr_ok then adddir(askfo.ddd.value);
end;

//copy - enter point
procedure tmainfo.on_copyfile(const sender: TObject);
var sl, sl2 : tstringlist;
    r : integer;
begin
  sl := tstringlist.create;
  sl2:= tstringlist.create;                    
  for r := 0 to wg_playlist.rowhigh do   
    if wg_playlist.datacols[0].selected[r] then
      begin
      sl.add(se_url[r]);
      sl2.add((sender as tmenuitem).caption + '/' + extractfilename(se_url[r]) );
      end;
  if (sender as tmenuitem).tag = 0 
     then copyfiles(sl, sl2, true)
     else copyfiles(sl, sl2, false);
  sl.free;
  sl2.free;
end;  

// SAVE GROUP
procedure tmainfo.on_saveinMyPlayLists(const sender: TObject);
begin
 if wg_playlist.rowcount = 0 then exit;
 if show_ask(get_mainwindow,'Enter new playlist name','',false,true,true) = mr_ok
    then save_in_my_playlists(wg_playlist, se_url, setsfo.dd_playlistsdir.value + '/' + askfo.SE.text);
end; 

procedure tmainfo.on_save_as_radio_or_tv_chanel(const sender: TObject);
begin
  if (sender as tmenuitem).tag = 0 then tw_main.activepage := tp_radio
                                   else tw_main.activepage := tp_tv; 
  chanelfo.se_name.value := se_name[wg_playlist.row];
  chanelfo.se_url.value := se_url[wg_playlist.row];
  chanelfo.fne_emblem.value := se_emblem[wg_playlist.row];
  chanelfo.se_key.value := se_name[wg_playlist.row]; //if save chanel we errase prev section
  chanelfo.bringtofront;
  if chanelfo.show = mr_ok then on_save_chanel(sender);
end;

procedure tmainfo.on_save_urls_to_clipboard(const sender: TObject);
var r : integer;
    s : string;
begin
  edit_.text := '';
  s := '';
  for r := 0 to wg_playlist.rowhigh do   //there is some bug with deleting strings in tstringgrid
  if wg_playlist.datacols[0].selected[r] then
      s := s + #10 + se_url[R];
 edit_.text := trim(s);
 edit_.editor.SELECTALL;
 edit_.editor.copytoclipboard;
end;

procedure tmainfo.on_save_as__(const sender: TObject);
var sl, sl2 : tstringlist;
begin
  if wg_playlist.row < 0 then exit;
  fd.controller.filter := '';
  fd.controller.filename := se_name[wg_playlist.row];
  if fd.execute = mr_Ok then
     begin
       sl := tstringlist.create;
       sl2:= tstringlist.create;
       sl.add(se_url[wg_playlist.row]);
       sl2.add(fd.controller.filename);
       copyfiles(sl, sl2, true);
       sl.free;
       sl2.free;
     end;
end;

procedure tmainfo.on_save_files_to__(const sender: TObject);
var itm : tmenuitem;
begin
  if wg_playlist.row < 0 then exit;
  if show_ask(get_mainwindow,'Copy file(s) in ','',true,false,true) = mr_ok then
     begin
       itm := tmenuitem.create;
       itm.tag := 0;
       itm.caption := askfo.ddd.value;
       itm.state := [as_localcaption,as_localonexecute];
       itm.onexecute := @on_copyfile;
       pm_pl.menu.items[2].items[8].submenu.insert(2,itm);
       on_copyfile(itm);
     end;
end;

procedure tmainfo.on_movefiles_to__(const sender: TObject);
var itm : tmenuitem;
begin
  if wg_playlist.row < 0 then exit;
  if show_ask(get_mainwindow,'Move file(s) in ','',true,false,true) = mr_ok then
    begin
      itm := tmenuitem.create;
      itm.tag := 1;
      itm.caption := askfo.ddd.value;
      itm.state := [as_localcaption,as_localonexecute];
      itm.onexecute := @on_copyfile;
      pm_pl.menu.items[2].items[9].submenu.insert(2,itm);
      on_copyfile(itm);
    end;
end;
//RENAME FILE 
procedure tmainfo.on_rename_file_in_playlist(const sender: TObject);
begin
 if wg_playlist.row > -1 then 
    rename_file(se_url[wg_playlist.row],true, true);   
end;
//DELETE MAINPROCEDURE: from playlist (selected, not selected, from disk)
procedure tmainfo.on_deleteformplaylist_pm(const sender: TObject);
var r, i, y : integer;
    delfiles, delfilesresult, selected: boolean;
    s : string;
    a : array of integer;
begin
  if wg_playlist.row < 0 then exit;
  r := 0;
  delfiles := false;
  if (sender as tmenuitem).tag = 2 then
   begin
     if show_ask(get_mainwindow,'Действительно хотите удалить файл(ы) с диска ?','',false,false,true) <> mr_ok then exit;
     delfiles := true;
   end;

  if (sender as tmenuitem).tag = 5 
     then selected := false //delete selected files
     else selected := true; //delete not selected files
  
  y := 0;
  for i := 0 to wg_playlist.rowhigh do   //there is some bug with deleting strings in tstringgrid
  if wg_playlist.datacols[0].selected[i] = selected then 
  begin 
    inc(y);
    setlength(a, y);
    a[y-1] := i;
  end;
  s := player.currenttrack.filename;
  delfilesresult := true;  
  r := player.tracknum;
  for i := high(a) downto 0 do
  begin  
     if delfiles 
        then if not deletefile(se_url[ a[i]] ) 
                then delfilesresult := false;
     wg_playlist.deleterow(a[i]);
     player.playlist.delete(a[i]);
     if a[i] <= r then dec(r);
  end;
  player.tracknum := r; 
  if player.playing then 
     if s <> player.playlist[r] then begin player.stop;player.play;end;
  if (sender as tmenuitem).tag = 2 then
     begin
       if setsfo.be_showonlymediafiles.value 
       then readlist(dd_dir.value)
       else flv.readlist; 
     end; 
  if not delfilesresult 
     then show_ask(get_mainwindow,'Не все файлы удалены !','',false,false,false)
     else if (sender as tmenuitem).tag = 2 then 
     begin
       if setsfo.be_showonlymediafiles.value 
       then readlist(dd_dir.value)
       else flv.readlist; 
     end; 
end;

//               move track in top in playlist
procedure tmainfo.on_topTruckinPlaylist(const sender: TObject);
var r, y : integer;
begin
  y := -1;
  if wg_playlist.rowcount = 0 then exit; 
  for r := 0 to wg_playlist.rowhigh do   //there is some bug with deleting strings in tstringgrid
  if wg_playlist.datacols[0].selected[r] then
     begin 
       inc(y);
	   wg_playlist.moverow(r,y);
	   player.playlist.move(r,y);
     end;
  findcurrentruck(false);
end;

//               move track up in playlist
procedure tmainfo.on_upTrackInPlaylist(const sender: TObject);
var r : integer;
begin
  if wg_playlist.rowcount = 0 then exit; 
  for r := 0 to wg_playlist.rowhigh do   //there is some bug with deleting strings in tstringgrid
  if wg_playlist.datacols[0].selected[r] then 
     begin
       if r = 0 then exit;
	   wg_playlist.moverow(r,r - 1);   
	   player.playlist.move(r,r - 1);
     end;
  findcurrentruck(false);
end;

//               move track down in playlist
procedure tmainfo.on_downTrackInPlaylist(const sender: TObject);
var r : integer;
begin
  if wg_playlist.rowcount = 0 then exit; 
  for r := wg_playlist.rowhigh downto 0 do   //there is some bug with deleting strings in tstringgrid
  if wg_playlist.datacols[0].selected[r] then 
     begin
       if r = wg_playlist.rowhigh then exit;
	   wg_playlist.moverow(r,r + 1);
	   player.playlist.move(r,r + 1);
     end;
  findcurrentruck(false);
end;

//               move track in bottom in playlist
procedure tmainfo.on_bottomTrauckinPlaylist(const sender: TObject);
var r, y : integer;
begin
  if wg_playlist.rowcount = 0 then exit; 
  y := wg_playlist.rowhigh + 1;
  for r := wg_playlist.rowhigh downto 0 do   //there is some bug with deleting strings in tstringgrid
  if wg_playlist.datacols[0].selected[r] then
     begin
       dec(y);
	   wg_playlist.moverow(r,y);
	   player.playlist.move(r,y);
     end;
  findcurrentruck(false);
end;

//               shuffle playlist
procedure tmainfo.on_shuffle(const sender: TObject);
var r, c, p : integer;
begin
  c := wg_playlist.rowhigh;
  if c > 0 then 
  for r := 0 to c do
   begin
     p := random(c);
     wg_playlist.moverow(r,p);
	 player.playlist.move(r,p);
   end;
  findcurrentruck(false);
end; 

//               sort playlist 
procedure tmainfo.on_sort_playlist(const sender: TObject);
var i : integer;
begin
  if wg_playlist.rowhigh < 0 then exit;
  wg_playlist.sort;
  player.playlist.clear;
  for i := 0 to wg_playlist.rowhigh do
      player.playlist.add(se_url[i]);
  findcurrentruck(false);
end;

//               clear playlist 
procedure tmainfo.on_clearplaylist(const sender: TObject);
begin
  wg_playlist.clear;
  player.playlist.clear;
  player.tracknum := -1;
end;









/// dblclick on playlist
procedure tmainfo.on_dblclick_on_playlist(const sender: TObject;
               var info: celleventinfoty);
begin
 if iscellclick(info,[ccr_dblclick]) 
    then begin
           findcurrentruck(true);
           player.trackNum := wg_playlist.row;
           player.play;
         end;
end;

//PLAYER EVENTS
// ======================================================= 
//               change volume event
// ======================================================= 
procedure tmainfo.on_changeVolumeEvent(const Sender: TObject;avolume: integer); 
begin
  if avolume = 0 then b_volume.imagenr := 11
    else if (avolume > 0) and (avolume <= 25) then b_volume.imagenr := 12
    else if (avolume > 25) and (avolume <= 50) then b_volume.imagenr := 13
    else if (avolume > 50) and (avolume <= 75) then b_volume.imagenr := 14
    else if (avolume > 75) then b_volume.imagenr := 15;
    
  sl_volume.value := avolume / 100;
end;
// ======================================================= 
//               connecting event
// ======================================================= 
procedure tmainfo.onConnectingEvent(const Sender: TObject; msg: AnsiString);
var i : integer;
begin
  if player.mode = __webcamera then 
   begin
     if viewfo.visible 
      then viewfo.ww_video.visible := true
      else mainfo.ww_video.visible := true;
    (*
    if b_windowstyle.tag = 0 then *)
    //mainfo.ww_video.visible := true //windows bag fix
(*         else 
         viewfo.ww_video.visible := true; //windows bag fix
         *)
   end
  else
  if player.mode = __cd then 
   begin
   end
  else
  if wg_playlist.rowcount > 0 then
  BEGIN
   for i := 0 to wg_playlist.rowhigh do 
   begin
   if i = player.tracknum 
      then begin
           se_cur[i] := _cursor;
           wg_playlist.rowfontstate[i] := 0;
           end
      else begin
           se_cur[i] := '';
           wg_playlist.rowfontstate[i] := -1;
           end;
   end;   
   if se_emblem[player.tracknum] > '' then 
   begin
   if fileexists(se_emblem[player.tracknum])then 
     try
      mainfo.im_emblem.bitmap.clear; 
      mainfo.im_emblem.bitmap.loadfromfile(se_emblem[player.tracknum]); 
      mainfo.im_emblem.visible := true;
      mainfo.b_animate.visible := false; 
     except
      mainfo.im_emblem.visible := false;
      mainfo.b_animate.visible := true;
     end;
   end
  END; 
 
  l_video_inf.caption := msg;
  b_play.imagenr := 4;
  b_playDVD.imagenr := 4;
end;
// ======================================================= 
//               end of track event
// ======================================================= 
procedure tmainfo.on_EndOfTrackEvent(const Sender: TObject; msg: AnsiString);
begin
  l_video_inf.caption := msg;
  l_trackinfo.caption := '';
  caption := 'XELPLAYER';
  l_video_inf.frame.caption := '';
  
  if b_windowstyle.tag = 1
    then begin
         viewfo.ww_video.visible := false;
         if viewfo.be_autoclose.value then viewfo.visible := false;
         end
    else mainfo.ww_video.visible := false;
 
    
  mainfo.im_emblem.visible := false;
  mainfo.b_animate.visible := true;
  //s_video.color := $171717;
  b_play.imagenr := 1;
  b_playDVD.imagenr := 1;
  sl_position.value := 0;
  if player2.playing then 
     begin
        if audioN = 1 then on_set_audio_0(sender); //first switch to audio0
        player2.stop;                              //then stop
        player2.playlist.clear;
     end;
end;

procedure tmainfo.on_ErrorEvent(const Sender: TObject; errormsg: AnsiString);
begin
 l_video_inf.caption := errormsg;
end;
// ======================================================= 
//               get debug message
// ======================================================= 
procedure tmainfo.on_GetDebugMessage(const Sender :TObject; msg : AnsiString);
var i, r : integer;
    sl_debug : tstringlist;
begin
    sl_debug := tstringlist.create;
    sl_debug.text := msg;
    if sl_debug.count > 0 then 
    for i := 0 to sl_debug.count - 1 do
    if setsfo <> nil then
    begin
      r := setsfo.sg_debug.rowcount;
	  setsfo.sg_debug.rowcount := r + 1;
	  setsfo.sg_debug[0][r] := sl_debug[i];
	  setsfo.sg_debug.row := r;
	end;
    sl_debug.free;
end;
// ======================================================= 
//               pause event
// ======================================================= 
procedure tmainfo.on_PauseEvent(const Sender: TObject; const apausing: Boolean;msg: AnsiString);
begin
  l_video_inf.caption := msg;
  if apausing then b_play.imagenr := 1 else b_play.imagenr := 4;
  if apausing then b_playDVD.imagenr := 1 else b_playDVD.imagenr := 4;
     
  if player2.playing then begin player2.pause;end;
end;
// ======================================================= 
//               playing event
// ======================================================= 
procedure tmainfo.on_PlayingEvent(const Sender: TObject; position: Integer;length: Integer; msg: AnsiString);
begin
  if length = 0 
     then sl_position.value := 0
     else sl_position.value := position / length;

  l_pos.caption := SecondsToFmtStr(position) + ' / ' + SecondsToFmtStr(length);
  l_video_inf.caption := msg;
end;
// ======================================================= 
//               start play event
// ======================================================= 
procedure tmainfo.on_StartPlayEvent(const Sender: TObject; videoW: Integer;videoH: Integer; msg: AnsiString);
var I : integer;
    s : string;
    f : textfile;
begin
  l_video_inf.caption := msg;
  if player.mode = __cd then 
   begin
   for i := 0 to player.playlist.count - 1 do 
      if i <> player.tracknum
         then sg_cdplaylist.datacols[0][i] := ''
         else sg_cdplaylist.datacols[0][i] := _cursor;
   end;

  if player.havevideostream then 
     begin
       mainfo.im_emblem.visible := false;
       mainfo.b_animate.visible := false;
       {$ifdef mswindows}
       application.processmessages;//strange bug in windows(winXP ???), without it window hungs
       {$endif}
       on_resizevideo(sender);

     end;
  IF PLAYER2.PLAYLIST.COUNT > 0 
     THEN begin 
            on_set_audio_1(sender);
            player2.play;
         end;
  l_trackinfo.caption := 
              'File    :  ' + player.currenttrack.filename + #10 +
              'Artist  :  ' + player.currenttrack.Artist + #10 +
              'Title   :  ' + player.currenttrack.Title + #10 +
              'Album   :  ' + player.currenttrack.Album + #10 +
              'Year    :  ' + player.currenttrack.Year + #10 +
              'Comment :  ' + player.currenttrack.Comment + #10 +
              'Genre   :  ' + player.currenttrack.Genre + #10 + 
              'Video   :  ' + inttostr(player.currenttrack.video.width) + ' x ' + inttostr(player.currenttrack.video.height);
  {$ifdef mswindows}
  application.processmessages; //strange bug in windows, without it window hungs
  {$endif}
  if tw_playlist.ACTIVEPAGE = tp_audiocd_pl
  then
  BEGIN
  caption := 'XELPLAYER : ' + sg_cdplaylist[1][player.tracknum];
  l_video_inf.frame.caption := '(' + sg_cdplaylist[1][player.tracknum] + ')';
{  if se_url_last[0] <> player.currenttrack.filename 
  then begin
       wg_last.insertrow(0);
       se_name_last[0] := sg_cdplaylist[1][player.tracknum];//extractfilename(player.currenttrack.filename);
       se_url_last[0] := sg_cdplaylist[2][player.tracknum];
       se_emblem_last[0] := '';
       assignfile(f,dirtowin(setsfo.dd_playlistsdir.value) + '__LAST_PLAYED__');
       if not fileexists(dirtowin(setsfo.dd_playlistsdir.value) + '__LAST_PLAYED__') then rewrite(f) else append(f);
       writeln(f,se_name_last[0]);
       writeln(f,se_url_last[0]);
       writeln(f,se_emblem_last[0]);
       closefile(f);
       end;
}  END
  ELSE
  BEGIN
  caption := 'XELPLAYER : ' + se_name[player.tracknum];
  l_video_inf.frame.caption := '(' + se_name[player.tracknum] + ')';
  if se_url_last[0] <> player.currenttrack.filename 
  then begin
       wg_last.insertrow(0);
       se_name_last[0] := se_name[player.tracknum];//extractfilename(player.currenttrack.filename);
       se_url_last[0] := se_url[player.tracknum];
       se_emblem_last[0] := se_emblem[player.tracknum];
       assignfile(f,dirtowin(setsfo.dd_playlistsdir.value) + '__LAST_PLAYED__');
       if not fileexists(dirtowin(setsfo.dd_playlistsdir.value) + '__LAST_PLAYED__') then rewrite(f) else append(f);
       writeln(f,se_name_last[0]);
       writeln(f,se_url_last[0]);
       writeln(f,se_emblem_last[0]);
       closefile(f);
       end;
  END;
end;
// ======================================================= 
//               player2 events
// ======================================================= 
procedure tmainfo.on_startplayevent2(const Sender: TObject; videoW: Integer; videoH: Integer; msg: AnsiString);
begin
  player2.position := player.position;
  on_set_audio_1(sender);
end;

procedure tmainfo.on_playingevent2(const Sender: TObject; position: Integer;length: Integer; msg: AnsiString);
begin
  if abs(position - player.position) > 1 then player2.position := player.position; //try sinhronize streams 0 and 1 by time
end;
// ======================================================= 
//               play 
// ======================================================= 
procedure tmainfo.on_play(const sender: TObject);
var i : integer;
begin
  if __continueplay then 
    begin
     __continueplay := false;
	      if tw_main.activepage = tp_TUNER then player.OpenTV()
	      else if tw_main.activepage = TP_WEBCAM then player.openwebcamera
	      else player.play(__continueplaypos);
    end
  else
  begin
	             writeln('update}}}}}}})');
  if player.playing
	 then begin 
	      player.pause;
	      if audioN = 1 then player2.pause;
	      end
	 else begin 
	      if tw_main.activepage = tp_TUNER then player.OpenTV()
	      else if tw_main.activepage = tp_audiocd then 
	          begin
                 {if player.tracknum < 1 then 
                 begin
                 player.tracknum := 1;
                 player.connecttoaudiocd;
                 player.OpenAudioCD(1);
                 end
                 else begin}
                      if player.playing then player.pause 
                       else begin
                 writeln('!!');
                            player.connecttoaudiocd;
                 writeln('!!');
                            if player.tracknum > sg_cdplaylist.rowcount - 1
                               then player.tracknum := 1;
                            player.OpenAudioCD(player.tracknum);
                            end;
                 //     end;
	          end
	      else if tw_main.activepage = TP_WEBCAM then player.openwebcamera
	      else begin 
	             if wg_playlist.rowcount > 0 then //restore playlist
	             begin
	             player.playlist.clear;
	             for i := 0 to wg_playlist.rowhigh do 
	                
	             end;
	             findcurrentruck(true);
	             player.play;
	             if audioN = 1 then player2.play;
	           end;
	      end;	 
  end;
end;
// ======================================================= 
//               stop
// ======================================================= 
procedure tmainfo.on_stop(const sender: TObject);
begin
  player.stop;
end;
// ======================================================= 
//               prev 
// ======================================================= 
procedure tmainfo.on_prev(const sender: TObject);
begin
  player.prev; 
end;
// ======================================================= 
//               next 
// ======================================================= 
procedure tmainfo.on_next(const sender: TObject);
begin
  player.next;
end;

// ======================================================= 
//               	EXTERNAL AUDIO
// ======================================================= 
procedure tmainfo.on_load_audio_1(const sender: TObject);
begin
  set2fo.visible := false; // if click button "Load external audio" on setfo 
  fd.controller.filter := '';
  if fd.execute = mr_Ok then
  begin
    player2.stop;
    player2.playlist.clear;
    player2.playlist.add(fd.controller.filename);
    IF PLAYER.PLAYing THEN player2.play;
    //on_set_audio_1(sender);  //no effect here, moved to player2.onstartplayevent
  end;
end;

procedure tmainfo.on_set_audio_1(const sender: TObject);
var i : integer;
begin
  if player2.playlist.count = 0 then exit;
  audioN := 1;
  i := player.volume;
  player.volume := 0;
  player2.volume := i;  
  ber_audio1.value := true;
end;

procedure tmainfo.on_set_audio_0(const sender: TObject);
var i : integer;
begin
  audioN := 0;
  if player2.playlist.count = 0 then exit;
  i := player2.volume;
  player2.volume := 0;
  player.volume := i;
  ber_audio0.value := true;  
end;
//sliders
procedure tmainfo.on_change_position(const sender: TObject);
var s : string;
begin
 s := (sender as tslider).name;
 delete(s,1,2);
 (findcomponent('pb' + s) as tprogressbar).value := (sender as tslider).value;
end;
// ======================================================= 
//               set position
// ======================================================= 
procedure tmainfo.on_setposition(const sender: TObject; var avalue: realty; var accept: Boolean);
begin
  player.position := round(avalue * player.length); 
end;
procedure tmainfo.on_setvolume(const sender: TObject; var avalue: realty; var accept: Boolean);
begin
  if audioN = 1
     then player2.volume := round(avalue * 100)
     else player.volume := round(avalue * 100);
end;
// ======================================================= 
//               mute on / off 
// ======================================================= 
procedure tmainfo.on_mute(const sender: TObject);
begin
  if audioN = 1 then player2.mute
                else player.mute;
end;

procedure tmainfo.on_showVsetform(const sender: TObject);
var  i : integer;
begin
  
//  if not set2fo.visible then
  if __showset2fofirst then
    begin
      __showset2fofirst := false;
      set2fo.left := left + width - set2fo.width;
      i  := top + height - set2fo.height - s_bottom.height;
      if i < 0 then set2fo.top := 0 else set2fo.top := i;
    end;
  set2fo.bringtofront;
  if set2fo.visible then set2fo.visible := false else set2fo.show;//(true);
end;
// ======================================================= 
//               set2 form settings
// ======================================================= 
// ======================================================= 
//               VIDEO SETTINS
// ======================================================= 
procedure tmainfo.on_setbrightness(const sender: TObject; var avalue: realty; var accept: Boolean);
begin
  player.video_brightness := round(avalue * 200 - 100);
end;

procedure tmainfo.on_setcontrast(const sender: TObject; var avalue: realty;
               var accept: Boolean);
begin
  player.video_contrast := round(avalue * 200 - 100);
end;

procedure tmainfo.on_setsaturation(const sender: TObject; var avalue: realty; var accept: Boolean);
begin
  player.video_saturation := round(avalue * 200 - 100);
end;

procedure tmainfo.on_sethue(const sender: TObject; var avalue: realty;var accept: Boolean);
begin
  player.video_hue := round(avalue * 200 - 100);
end;

procedure tmainfo.on_setgamma(const sender: TObject; var avalue: realty;var accept: Boolean);
begin
  player.video_gamma := round(avalue * 200 - 100);
end;

procedure tmainfo.on_setbalance(const sender: TObject; var avalue: realty;var accept: Boolean);
begin
  player.balance := avalue * 2 - 1;
end;

procedure tmainfo.on_reset_video_options(const sender: TObject);
begin
 sl_brightness.value := 0.5;  player.video_brightness := 0;
 sl_contrast.value := 0.5;    player.video_contrast := 0;
 sl_gamma.value := 0.5;       player.video_gamma := 0;
 sl_hue.value := 0.5;         player.video_hue := 0;
 sl_saturation.value := 0.5;  player.video_saturation := 0;
end;

procedure tmainfo.on_set_resolution(const sender: TObject);
begin  
 if (sender as tbooleaneditradio).value = true then on_resizevideo(sender);
end;

// ======================================================= 
//               	equalizer
// ======================================================= 
procedure tmainfo.on_set_equalizer(const sender: TObject);
begin
  player.equalizer := (sl_eq1.frame.caption) + ':' + 
                      (sl_eq2.frame.caption) + ':' +
                      (sl_eq3.frame.caption) + ':' +
                      (sl_eq4.frame.caption) + ':' +
                      (sl_eq5.frame.caption) + ':' +
                      (sl_eq6.frame.caption) + ':' +
                      (sl_eq7.frame.caption) + ':' +
                      (sl_eq8.frame.caption) + ':' +
                      (sl_eq9.frame.caption) + ':' +
                      (sl_eq10.frame.caption);
  player2.equalizer := player.equalizer;
end;

procedure tmainfo.on_set_equalizer_row(const sender: TObject;var avalue: realty; var accept: Boolean);
begin
  (sender as tslider).frame.caption := floattostr( round((avalue - 0.5)  * 24) );
  on_set_equalizer(sender);
end;

procedure tmainfo.on_cancel_equalizer(const sender: TObject);
begin
  sl_eq1.value := 0.5; sl_eq1.frame.caption := '0';
  sl_eq2.value := 0.5; sl_eq2.frame.caption := '0';
  sl_eq3.value := 0.5; sl_eq3.frame.caption := '0';
  sl_eq4.value := 0.5; sl_eq4.frame.caption := '0';
  sl_eq5.value := 0.5; sl_eq5.frame.caption := '0';
  sl_eq6.value := 0.5; sl_eq6.frame.caption := '0';
  sl_eq7.value := 0.5; sl_eq7.frame.caption := '0';
  sl_eq8.value := 0.5; sl_eq8.frame.caption := '0';
  sl_eq9.value := 0.5; sl_eq9.frame.caption := '0';
  sl_eq10.value := 0.5; sl_eq10.frame.caption := '0';
  on_set_equalizer(sender);
end;

procedure tmainfo.on_set_audioN(const sender: TObject; var avalue: Boolean;var accept: Boolean);
begin
 if player2.playlist.count = 0 then begin accept := false; exit; end;
 if not player2.playing then begin accept := false; exit; end;
 
 if sender = ber_audio0 
    then on_set_audio_0(sender)
    else on_set_audio_1(sender);
end;

procedure tmainfo.on_set_volnorm(const sender: TObject; var avalue: Boolean;
               var accept: Boolean);
begin
  player.AudioEffect_volumeNorm := avalue;
end;

procedure tmainfo.on_set_karaoke(const sender: TObject; var avalue: Boolean;
               var accept: Boolean);
begin
  player.AudioEffect_karaoke := avalue;
end;

procedure tmainfo.on_set_extrastereo(const sender: TObject; var avalue: Boolean;
               var accept: Boolean);
begin
  player.AudioEffect_extrastereo := avalue;
end;

procedure tmainfo.on_set_vectors(const sender: TObject; var avalue: Boolean;var accept: Boolean);
begin
  player.videoeffect_vectors := avalue;
  restart_play;
end;

procedure tmainfo.on_set_videoeffect(const sender: TObject; var avalue: Boolean;var accept: Boolean);
begin
  if sender = b_mirror then player.videoeffect_mirror := avalue;
  if sender = b_flip then   player.videoeffect_flip := avalue;
  if sender = b_rotate90 then player.videoeffect_rotate90 := avalue;
  restart_play; 
end;

procedure tmainfo.restart_play;  //need check it!!!
var pos_, a : string;
begin
  if player.playing then
    begin
      pos_ := SecondsToFmtStr(player.position);
      a := '';
      if player2.playlist.count > 0 then a := player2.playlist[0];
      on_stop(b_stop);
      player.playfrompositionStr := pos_;
      on_play(b_play);
      player.playfrompositionStr := '';
      if a > '' then
        begin
          player2.playlist.add(a);
          player2.playfrompositionStr := pos_;
          player2.play;
          player2.playfrompositionStr := '';
        end;      
    end;
end;

procedure tmainfo.on_maintimer(const sender: TObject);
var f : textfile;
    s, s2 : string;
    sl, sl2 : tstringlist;
    plfile : string;
    
    
procedure addfiles;
var y : integer;
begin
   if sl.count > 0 then 
        for y := 0 to sl.count - 1 do 
            addfile(EXTRACTFILENAME(sl[y]), sl[y], '');
end;   

procedure addfiles2;
var y : integer;
begin
   if sl.count > 0 then 
        for y := 0 to sl.count - 1 do 
             addfile(sl2[y], sl[y], '');
end;   

begin

 //if b_fullscreen.tag = 1 then mainfo.ww_video.cursor := cr_none;
 
 if (setsfo.be_useonecopy.value)or(b_useone) then 
 begin
 b_useone := false;

 S := sys_getuserhomedir;
 {$ifdef windows} IF S > '' THEN IF S[1] = '/' THEN DELETE(S,1,1);{$endif}
 forcedirectories(S + '/.Almin-Soft/xelplayer');
 plfile := S + '/.Almin-Soft/xelplayer/xelplayer.pl';

 if fileexists(plfile) then
   begin
     on_clearplaylist(sender);

     assignfile(f,plfile);
     reset(f);
     sl := tstringlist.create;
     sl2 := tstringlist.create;
     while not eof(f) do
       begin
         readln(f,s);
         if lowercase(extractfileext(s)) = '.pls' then 
           begin
            openpls_playlist(s, sl);
            addfiles;
           end 
         else if lowercase(extractfileext(s)) = '.m3u' then 
           begin
             openm3u_playlist(s, sl, sl2);
             addfiles2;
           end
         else if lowercase(extractfileext(s)) = '.kpl' then 
           begin
             openkpl_playlist(s, sl);
             addfiles;
           end
         else if lowercase(extractfileext(s)) = '.asx' then 
           begin
             //openasx_playlist(s, sl);
             //addfiles;
           end
         else if extractfileext(s) = '.online' then
           begin
              openOnline_playlist(s, sl2, sl);
              addfiles;
           end
         else addfile(extractfilename(s),s, '');

       end;
     sl.free;  
     sl2.free;  
     closefile(f);
     deletefile(plfile);
     on_stop(sender);
     on_play(sender);
   end;
  END;
end;

procedure tmainfo.on_open_stop_dvd(const sender: TObject);
begin
 __continueplay := false;
 if (player.playing)and(player.mode <> __dvd)then player.stop;
 if (player.playing)and(player.mode = __dvd)
    then player.stop
    else begin
           if tbooleaneditradio2.value
             then player.aditionalparams := '-dvd-device "' + ddd_dvdpath.value + '"'
             else player.aditionalparams := '';
           player.opendvd;
         end;
end;

procedure tmainfo.on_dvd_menu(const sender: TObject);
begin
  player.dvdnavigation_menu;
end;

procedure tmainfo.on_dvd_left(const sender: TObject);
begin
  player.dvdnavigation_left;
end;

procedure tmainfo.on_dvd_right(const sender: TObject);
begin
  player.dvdnavigation_right;
end;

procedure tmainfo.on_dvd_up(const sender: TObject);
begin
  player.dvdnavigation_up;
end;

procedure tmainfo.on_dvd_down(const sender: TObject);
begin
  player.dvdnavigation_down;
end;

procedure tmainfo.on_dvd_select(const sender: TObject);
begin
  player.dvdnavigation_select;
  if player.pausing then player.pause;
end;

procedure tmainfo.on_dvdpause(const sender: TObject);
begin
  player.pause;
end;

procedure tmainfo.on_set_dvd_iso_path(const sender: TObject;
               var avalue: Boolean; var accept: Boolean);
begin
  ddd_dvdpath.enabled := avalue;
  ddd_audioCDdevice.enabled := not avalue;
  trichbutton5.enabled := not avalue;
end;

// ======================================================= 
//               TV - TUNER
// =======================================================
function StrToTvNorm(NormStr:string):tvnorm;
var I:tvnorm;
begin
Result:=SECAM_DK;
for I in TvNorm do
 if Tvstr[I]=NormStr then Result:=I;
end;

procedure tmainfo.loadtvtuner;
var i : integer;
    Itv : TTVData;
    str : string;
begin
  (* TV - tuner *)
 sc:=TIniFile.Create(CFGChannelsName);
 for i:=1 to sc.ReadInteger('TVcount','Count',0) do
  begin
  Itv.Freq:=sc.ReadInteger('TV'+inttostr(i),'freqtv',45);
  str:=Sc.ReadString('TV'+inttostr(i),'normtv','SECAM-DK');
  Itv.Norm:=StrToTvNorm(str);
  Itv.Name:=sc.ReadString('TV'+inttostr(i),'tvname','TV');
  player.ListofChanell.AddItem(Itv);
  st.rowcount := st.rowcount + 1;
  st.datacols[0][st.rowcount-1]:=Itv.Name;
  st.datacols[1][st.rowcount-1] :=inttostr(Itv.Freq) ;
  st.datacols[2][st.rowcount-1]:=str;
 end;
 sc.free;
end;

procedure tmainfo.on_dblclickOnTVlist(const sender: TObject; var info: celleventinfoty);
var sf : focuscellactionty;
     t : twidgetgrid;
begin
 if iscellclick(info,[ccr_dblclick])      // if duble click ...
  then
  begin //Set Channel
    player.SetChannelByfreq(player.ListofChanell.GetData(CountSelectionGrid).Freq);
    player.SetChannelNorm(player.ListofChanell.GetData(CountSelectionGrid).Norm);
  end;
sf := info.selectaction;
t  := twidgetgrid(Sender);
if (sf = fca_focusinforce) or (sf = fca_focusin) or (sf = fca_entergrid) then
begin
if (info.cell.col <= t.datacols.Count) then
 begin
  if (info.cell.col >= 0) then
  begin
   if (info.cell.row <= t.rowcount) then
   begin
    if (info.cell.row >= 0) then
    begin     //выделенная ячейка
     CountSelectionGrid := info.cell.row;
     e_tvname.value:=st.datacols[0][CountSelectionGrid];
     e_freqtv.value:=StrToInt(st.datacols[1][CountSelectionGrid]);
     e_normtv.value:=st.datacols[2][CountSelectionGrid];
    end;
   end;
  end;
 end;
end;
end;

//Добавить канал
procedure tmainfo.on_addchanell(const sender: TObject);
var tvd:TTVData;
begin
tvd.Freq:=e_freqtv.value;
tvd.Norm:=StrToTvNorm(e_normtv.value);
tvd.Name:=e_tvname.value;
player.ListofChanell.AddItem(tvd);
st.rowcount := st.rowcount + 1;
st.datacols[0][st.rowcount-1]:=e_tvname.value;
st.datacols[1][st.rowcount-1] :=inttostr(e_freqtv.value) ;
st.datacols[2][st.rowcount-1]:=e_normtv.value;
SaveChannel();
end;

//Удалить канал
procedure tmainfo.on_deletechanell(const sender: TObject);
begin
st.deleterow(st.row);
player.ListofChanell.RemoveItem(CountSelectionGrid);
SaveChannel();
end;

procedure tmainfo.SaveChannel();
var i:integer;
begin
sc:=TIniFile.Create(CFGChannelsName);
sc.WriteInteger('TVcount','Count',mainfo.st.rowcount);
for i:=1 to player.ListofChanell.Count do
begin
sc.WriteString('TV'+inttostr(i),'tvname',player.ListofChanell.GetData(i-1).Name);
sc.WriteInteger('TV'+inttostr(i),'freqtv',player.ListofChanell.GetData(i-1).Freq);
Sc.WriteString('TV'+inttostr(i),'normtv',tvstr[player.ListofChanell.GetData(i-1).Norm]);
end;
sc.WriteInteger('TVcount','Count',mainfo.st.rowcount);
sc.free;
end;

procedure tmainfo.on_setchanneltv(const sender: TObject);
var idata:TTVData;
begin
st.datacols[0][CountSelectionGrid]:=e_tvname.value;
st.datacols[1][CountSelectionGrid] :=inttostr(e_freqtv.value) ;
st.datacols[2][CountSelectionGrid]:=e_normtv.value;

idata.Name:=e_tvname.value;
idata.Freq:=e_freqtv.value ;
idata.Norm:=StrToTvNorm(e_normtv.value);
player.stop;
player.ListofChanell.SetData(CountSelectionGrid,idata);
player.opentv;
player.SetChannelByfreq(player.ListofChanell.GetData(CountSelectionGrid).Freq);
player.SetChannelNorm(player.ListofChanell.GetData(CountSelectionGrid).Norm);
SaveChannel();
end;

procedure tmainfo.on_set_webcam2(const sender: TObject; var avalue: msestring;
               var accept: Boolean);
begin
  setsfo.ddd_webcam.value := avalue;
  setsfo.on_setwebcameradevice(sender, avalue, accept);
end;

procedure tmainfo.on_updatedivices2(const sender: TObject);
begin 
  setsfo.on_update_devices(sender);
end;

procedure tmainfo.on_setplayplaylist_up_down(const sender: TObject);
begin
 case b_forward.tag of
 0 : begin
       player.playplaylistback := TRUE;
       b_forward.imagenr := 26;
       b_forward.tag := 1;
     end;
 1: begin 
       player.playplaylistback := FALSE;
       b_forward.imagenr := 25;
       b_forward.tag := 0;
    end;
  end;
end;

procedure tmainfo.on_setrepeatplaylist(const sender: TObject);
begin
 case b_repeat.tag of
 0 : begin
       player.repeatplaylist := TRUE; 
       b_repeat.imagenr := 27;
       b_repeat.tag := 1;
     end;
 1: begin 
       player.repeatplaylist := FALSE;
       b_repeat.imagenr := 28;
       b_repeat.tag := 0;
    end;
  end;
end;

procedure tmainfo.on_getpreview_pl(const sender: TObject);
begin
  if wg_playlist.row < 0 then exit;
  getpreview(se_url[wg_playlist.row]);
end;

procedure tmainfo.getpreview(fname  : string);
var fs : tsearchrec;
    curdir : string;
    
procedure get__(I, t1 : integer; t2 : real);
begin
  player_preview.getpreview(fname, curdir, t1);
  WITH previewfo DO
  if findfirst(curdir + '*1.png', faanyfile, fs) = 0 
    then (FINDCOMPONENT('im' + INTTOSTR(I)) AS TIMAGE).bitmap.loadfromfile(curdir + fs.name)
    else (FINDCOMPONENT('im' + INTTOSTR(I)) AS TIMAGE).bitmap := im0.bitmap;
  deletefile(curdir + fs.name);
  previewfo.pb.value := t2;
  previewfo.pb.update;
end;    
    
begin
  curdir := dirtowin(getcurrentdir);
//  writeln('cd=',curdir, ' fnmae=',fname);

  previewfo.im1.bitmap.clear;
  previewfo.im2.bitmap.clear;
  previewfo.im3.bitmap.clear;
  previewfo.im4.bitmap.clear;
  previewfo.im5.bitmap.clear;
  previewfo.im6.bitmap.clear;
  previewfo.pb.value := 0;
  previewfo.pb.visible := true;
  previewfo.bringtofront;
  previewfo.show;
  previewfo.l_.caption := fname;
  application.processmessages;
  try
    get__(1,50,0.16);
    get__(2,100,0.33);
    get__(3,150,0.49);
    get__(4,200,0.66);
    get__(5,250,0.82);
    get__(6,300,1);
  previewfo.pb.visible := false;
  previewfo.bringtofront;
  previewfo.show;
  except
//     writeln('ERROR');
  end;
end;

procedure tmainfo.on_showdubble(const sender: TObject);
begin
  if dubblefo = nil then exit;
  dubblefo.left := mainfo.left + s_video.width{ + mainfo.frame.framewidth} - dubblefo.width + mainfoframewidth;
  dubblefo.top  := mainfo.top + s_video.height{ + mainfo.frame.framewidth} + s_top.height - dubblefo.height + mainfoframewidth + mainfoframewidth;
  if (b_library.tag = 1)and(b_windowstyle.tag = 0)
    then begin
         if not dubblefo.visible then dubblefo.show;
         end
    else dubblefo.visible := false;
end;

procedure tmainfo.on_deactivateform(const sender: TObject);
begin
  dubblefo.visible := false;
end;

procedure tmainfo.loadshemas;
var fs : tsearchrec;
     s : string;
begin
  setsfo.dd_sheme.dropdown.cols.clear;
  if findfirst(shemepath + '*.xelsheme', faanyfile, fs) = 0 then
  repeat
    s := fs.name;
    delete(s,system.pos('.xelsheme',s), length('.xelsheme'));
    setsfo.dd_sheme.dropdown.cols.addrow(msestring(s));
  until findnext(fs) <> 0;
  findclose(fs);
end;

procedure tmainfo.loadicons;
var fs : tsearchrec;
     s : string;
begin
  setsfo.dd_icons.dropdown.cols.clear;
  if findfirst(shemepath + 'icons/*', faanyfile, fs) = 0 then
  repeat
    s := fs.name;
    if (s='.')or(s='..')or(s='.') then continue;
    setsfo.dd_icons.dropdown.cols.addrow(msestring(s));
  until findnext(fs) <> 0;
  findclose(fs);
end;

procedure tmainfo.on_timer_animate(const sender: TObject);
begin
 inc(n_animate);
 if n_animate = im_animate.count * 4 then n_animate := 0;
 if n_animate < im_animate.count
   then mainfo.b_animate.imagenr := n_animate;
end;

procedure tmainfo.on_screenshot(const sender: TObject);
begin
  player.screenshot;
end;

procedure tmainfo.on_set_dvd_device(const sender: TObject; var avalue: Boolean;
               var accept: Boolean);
begin
  ddd_dvdpath.enabled := not avalue;
  ddd_audioCDdevice.enabled := avalue;
  trichbutton5.enabled := avalue;
end;

procedure tmainfo.on_set_cdrom_device3(const sender: TObject;
               var avalue: msestring; var accept: Boolean);
begin
  setsfo.ddd_audioCDdevice.value := avalue;
  setsfo.on_setaudiocddevice(sender, avalue, accept);
end;

procedure tmainfo.on_keyup_form(const sender: twidget;
               var ainfo: keyeventinfoty);
var r : realty;               
    i : integer;
begin
  if ainfo.key = key_Space then on_play(sender);
  if ainfo.key = key_M then on_mute(sender);
  if ainfo.key = key_Z then on_prev(sender);
  if ainfo.key = key_X then on_play(sender);
  if ainfo.key = key_C then on_stop(sender);
  if ainfo.key = key_V then on_next(sender);
  if ainfo.key = key_F then on_fullscreen(sender);
  if ainfo.key = key_L then on_showlibrary(b_library);
  if ainfo.key = key_S then on_changewindowstyle(b_windowstyle);
  if player.playing then
  //if  ainfo.eventkind = ek_keypress then 
    begin
    //writeln('debug on_keyupform event : key press ...', ainfo.key);
    //writeln(player.position);
    //writeln(player.length);
      //if b_library.tag = 1 then 
         begin
           i := player.position;
           if ainfo.key = key_Right then 
               if i + 5 <= player.length  then player.position := i + 5;
           if ainfo.key = key_Left then 
               if i - 5 >= 0  then player.position := i - 5;
           if player.length > 0 then 
               sl_position.value := player.position / player.length;
         end;
    end;
end;

procedure tmainfo.on_mousewheelform(const sender: twidget;var ainfo: mousewheeleventinfoty);
var r : realty;               
    i : integer;
begin
     if player.playing then 
       begin
//       writeln(sender.classname);
         i := player.volume;
         if ainfo.wheel= mw_up then 
           if i + 5 <= 100 then player.volume := player.volume + 5;
         if ainfo.wheel= mw_down then 
           if i - 5 >= 0 then player.volume := player.volume - 5;
       end;
end;

procedure tmainfo.on_get_icon(const sender: TObject; const ainfo: fileinfoty;
               var imagelist: timagelist; var imagenr: Integer);
//var 
//  magic: magic_t;
  
begin
//    magic:=magic_open( {MAGIC_SYMLINK or }MAGIC_MIME );
//    magic_load(magic, nil);
//    WriteLn(flv.directory + ainfo.name,'=',magic_file(magic, pchar(flv.directory + ainfo.name)));
//    magic_close(magic); 
 {$ifdef linux}
 //if fpReadLink(flv.directory + ainfo.name) <> ''  
 //then if directoryexists(flv.directory  + ainfo.name)
 //        then imagenr := 5 else imagenr := 4;
 {$endif}
end;

procedure tmainfo.on_show_flv_popup(const sender: TObject);
 var gr : mouseeventinfoty;
begin
  gr.pos.x := 0;
  gr.pos.y := b_menuflv.height;
  pm_flv.show(b_menuflv, gr);
end;

procedure tmainfo.on_show_flv_menu(const sender: tcustommenu);
var itm : tmenuitem;
     fs : tsearchrec;
      i,y : integer;
    path: string;
    sl  : tstringlist;
      s : string;
 Drives : Array[1..33] of String;    
 count  : integer;  
begin
  pm_flv.menu.items[3].submenu.clear;
  {$ifdef windows}
  Try   //cdrom example from /usr/lib/fpc/src/packages/cdrom/examples
    Count:={GetCDRomDevices}GetLogicalDriveStrings(Drives);
    if count > 33 then count := 33;
    For I:=1 to count do begin
//        ddd_audioCDdevice.dropdown.cols.addrow(msestring(extractfilename(Drives[i]))); 
//        mainfo.ddd_audioCDdevice.dropdown.cols.addrow(msestring(extractfilename(Drives[i]))); 
          itm := tmenuitem.create;
          itm.tag := 0;
          itm.caption := Drives[i];
          itm.hint := Drives[i] + '/';
          
          itm.state := [as_localcaption,as_localonexecute];
          itm.onexecute := @on_open_dir_pm;
          pm_flv.menu.items[3].submenu.insert(0,itm);

      end;
  Except
    On E : exception do
      begin 
      end;
  end; 
  {$endif}
  {$ifdef linux}
  {for i := 0 to 2 do
  BEGIN
  case i of
    0 : path := '/run/media/' + whoami + '/';
    1 : path := '/mnt/';
    2 : path := '/media/';
  end;
  if findfirst(path + '*', faanyfile, fs)= 0
  then repeat
       if (fs.name = '')or(fs.name = '.')or(fs.name = '..') then continue;
       if (fadirectory and fs.attr) <> 0 then 
          begin
            itm := tmenuitem.create;
            itm.tag := 0;
            itm.caption := fs.name;
            itm.hint := path + fs.name;
            
            itm.state := [as_localcaption,as_localonexecute];
            itm.onexecute := @on_open_dir_pm;
            pm_flv.menu.items[3].submenu.insert(0,itm);
          end;         
       until findnext(fs) <> 0;
  END;
  findclose(fs);
  }
  
  sl  := tstringlist.create;
  sl.loadfromfile('/etc/mtab');
  if sl.count > 0 then
  for i := 0 to sl.count - 1 do
    begin
      s := trimleft(sl[i]);
      if system.pos('/dev/',s) = 1 then 
        begin
          delete(s,1,system.pos(' ',s));
          s := copy(trim(s), 1, system.pos(' ',s) - 1);
          if trim(s) = '/' then continue;
          while system.pos('\040',s) > 0 do 
            begin
              y := system.pos('\040',s);
              delete(s,y, length('\040'));
              insert(' ',s,y);
            end;
          itm := tmenuitem.create;
          itm.tag := 0;
          itm.caption := extractfilename(s);
          itm.hint := s + '/';
          
          itm.state := [as_localcaption,as_localonexecute];
          itm.onexecute := @on_open_dir_pm;
          pm_flv.menu.items[3].submenu.insert(0,itm);
        end;
    end;
  sl.free;  
  {$endif} 
end;

procedure tmainfo.on_open_dir_pm(const sender: TObject);
var s : string;
begin
  s := dd_dir.value;
  dd_dir.value := (sender as tmenuitem).hint;
  if not setsfo.be_showonlymediafiles.value
  then begin
       try
       flv.directory := (sender as tmenuitem).hint;
       flv.readlist;
       except
         dd_dir.value := s;
         flv.directory := s;
         flv.readlist;
         show_ask(mainfo,'Drive not ready', '', false,false,false);
       end;
       end
  else readlist((sender as tmenuitem).hint);
end;

procedure tmainfo.loadlast(dir : string);
var sl : tstringlist;
     i : integer;
     f : textfile;
     s1,s2,s3 : string;
begin
{ sl := tstringlist.create;
 openpls_playlist(dir + '__LAST_PLAYED__', sl); 
 filechangenotifyer.path := dir + '__LAST_PLAYED__';
 wg_last.rowcount := sl.count;
 if sl.count > 0 then 
   for i := 0 to sl.count - 1 do
   begin
     se_name_last[i] := extractfilename(sl[i]);
     se_url_last[i] := sl[i];
   end;
 sl.free;  
}
 assignfile(f,dir + '__LAST_PLAYED__');
 if not fileexists(dir + '__LAST_PLAYED__') then rewrite(f) else reset(f);
 while not eof(f) do 
   begin
     readln(f,s1);
     readln(f,s2);
     readln(f,s3);
     wg_last.insertrow(0);
//     wg_last.rowcount := wg_last.rowcount + 1;
     se_name_last[0] := s1;
     se_url_last[0] := s2;
     se_emblem_last[0] := s3;
   end;
 closefile(f);
end;

procedure tmainfo.on_last_played_change(const sender: tfilechangenotifyer;
               const info: filechangeinfoty);
begin
  loadlast(dirtowin(SETSFO.dd_playlistsdir.value));
end;

procedure tmainfo.on_createitemflv(const sender: tcustomitemlist;
               var item: tlistedititem);
var s : string;
begin
  //writeln(flv.items[sender.indexof(item) - 1].caption);
  //writeln(sender.items[sender.indexof(item)].classname);
  item.free;
  //item:= tlistedititem.create;
  //item.caption := '11';
  //if item = nil then writeln('!!!');
  //writeln(flv.directory + item.caption);
  //s := DetectMimeTypeFromFile(flv.directory + item.caption, filemagic);
  //if (system.pos('video',s) = 0)and(system.pos('audio',s) = 0) then item.free;
end;

procedure tmainfo.on_ckeckfile(const sender: TObject;
               const streaminfo: dirstreaminfoty; const fileinfo: fileinfoty;
               var accept: Boolean);
begin
  
end;

procedure tmainfo.on_listread_flv(const sender: TObject);
var i : integer;
    s : string;
begin
{
  writeln(flv.itemlist.count);
  writeln(flv.rowcount);
  if flv.itemlist.count > 0 then
  for i := flv.itemlist.count - 1 downto 0 do 
    begin
      
      s := DetectMimeTypeFromFile(flv.directory + flv.items[i].caption, filemagic);
      if (system.pos('video',s) = 0)and(system.pos('audio',s) = 0)
      and(s <> 'inode/directory') then 
        begin 
          flv.deleterow(i);
          //writeln(i);
        end;
    end;
}
end;

procedure tmainfo.readlist(dir : string);
var fs : tsearchrec;
    r, i  : integer;
    s : string;
    sl1,sl2 : tstringlist;
begin  
  if not fileexists(filemagic) then
  begin
     show_ask(get_mainwindow,'magic file not found! Set path currectly.' + #10 + 
              '(Settings > XelPlayer > Path to magic.mgc)',
              '',false,false,false); 
  exit;
  end;              
  wg_fl.clear;
  wg_fl.rowcount := 1;
  se_fname[0] := 'Building, please wait ...';
  APPLICATION.PROCESSMESSAGES;
  sl1 := tstringlist.create;
  sl2 := tstringlist.create;
  if findfirst(dir + '*', faanyfile, fs) = 0 then
  repeat
     if (fs.name = '') or (fs.name = '.') or (fs.name = '..') then continue;
     if (fadirectory and fs.attr) <> 0 
      then sl1.add(dir + fs.name)
      else begin
             //if setsfo.be_showonlymediafiles.value 
             //then begin             
                  //s := DetectMimeFromFile(dir + fs.name, filemagic);
                  {$ifdef linux}
                  s := DetectMimeTypeFromFile(dir + fs.name, filemagic);
                  {$endif}
                  {$ifdef windows}
                  s := dir + fs.name;
                  {$endif}
//                  writeln(s);
                  if (system.pos('video',s) = 1)or(system.pos('audio',s) = 1)
                    then sl2.add(dir + fs.name);
             //     end
             //else sl2.add(dir + fs.name);
           end;
       //APPLICATION.PROCESSMESSAGES;    
  until findnext(fs) <> 0;
  findclose(fs);
  sl1.sort;
  sl2.sort;
  wg_fl.clear;
  wg_fl.rowcount := sl1.count + sl2.count;
  if sl1.count > 0 then 
   for i := 0 to sl1.count - 1 do
    begin
      se_fname[i] := extractfilename(sl1[i]);
      se_furl[i] := sl1[i] + '/';
      di_fl.gridvalue[i] := 0;
      application.processmessages;
    end;
  if sl2.count > 0 then 
   for i := 0 to sl2.count - 1 do
    begin
      se_fname[sl1.count + i] := extractfilename(sl2[i]);
      se_furl[sl1.count + i] := sl2[i];
      di_fl.gridvalue[sl1.count + i] := 2;
      application.processmessages;
    end;
  sl1.free;
  sl2.free;
end;

procedure tmainfo.on_fl_event(const sender: TObject; var info: celleventinfoty);
begin
   if iscellclick(info,[ccr_dblclick])then 
     begin
      if wg_fl.row < 0 then exit;
      if extractfilename(se_furl[wg_fl.row]) <> se_fname[wg_fl.row] 
      then begin
           dd_dir.value := se_furl[wg_fl.row];
           readlist(se_furl[wg_fl.row]);
           end
      else on_addfilefromdisk(b_open_from_flv);
     end;
end;

procedure tmainfo.on_getpreview_flv(const sender: TObject);
begin
  if flv.focusedindex < 0 then exit;
  getpreview(flv.directory + flv.items[flv.focusedindex].caption);
end;

procedure tmainfo.on_getpreview_last(const sender: TObject);
begin
  if wg_last.row < 0 then exit;
  getpreview(se_url_last[ wg_last.row ]);
end;

procedure tmainfo.on_showtrackinfo(const sender: TObject);
var fs : tsearchrec;
    curdir : string;
begin
  if wg_playlist.row < 0 then exit;
  curdir := dirtowin(getcurrentdir);
  writeln('se_url=',se_url[wg_playlist.row]);
  player_preview.getpreview(se_url[wg_playlist.row], curdir, 10);
//  infofo.l_.caption := se_url[wg_playlist.row];
  infofo.se_.value := se_url[wg_playlist.row];
//  writeln('l_=',infofo.l_.caption);
  writeln('se_=',infofo.se_.value);
  if findfirst(curdir + '*1.png', faanyfile, fs) = 0 
    then infofo.im_full.bitmap.loadfromfile(curdir + fs.name)
    else infofo.im_full.bitmap := previewfo.im0.bitmap;
  deletefile(curdir + fs.name);
    
  player_info.play(se_url[wg_playlist.row]);
  infofo.show;
  exit;
  if spl2.top = s_middle.height - spl2.height
    then spl2.top := height - 280
    else on_hide_addons(sender);
end;

procedure tmainfo.on_player_preview_startplay(const Sender: TObject;
               videoW: Integer; videoH: Integer; msg: AnsiString);
begin
  player_preview.stop;
end;

procedure tmainfo.on_player_info_tartplay_event(const Sender: TObject;
               videoW: Integer; videoH: Integer; msg: AnsiString);
begin
  infofo.l_trackinfo.caption := 
              'File    :  ' + player_info.currenttrack.filename + #10 +
              'Artist  :  ' + player_info.currenttrack.Artist + #10 +
              'Title   :  ' + player_info.currenttrack.Title + #10 +
              'Album   :  ' + player_info.currenttrack.Album + #10 +
              'Year    :  ' + player_info.currenttrack.Year + #10 +
              'Comment :  ' + player_info.currenttrack.Comment + #10 +
              'Genre   :  ' + player_info.currenttrack.Genre + #10 + 
              'Video   :  ' + inttostr(player_info.currenttrack.video.width) + ' x ' + inttostr(player_info.currenttrack.video.height);

  player_info.stop;
end;

// ======================================================= 
//               	DRAG AND DROP
// ======================================================= 
procedure tmainfo.on_beforedragbegin_pl(const asender: TObject; const apos: pointty;
                   var adragobject: tdragobject; var processed: Boolean);
var
 co1 : gridcoordty;
   r : integer;
   s : string;
   i : integer;
begin
{
 if (sender.classname = 'tstringgrid') then
 with tstringgrid(sender) do 
 begin
  if cellatpos(apos,co1) = ck_data then 
   begin
    s := '';
    for r := 0 to sg_playlist.rowhigh do   
    if sg_playlist[0].selected[r] then s := inttostr(r) + ',' + s;
    tstringdragobject.create(sender,dragobject,apos).data := s;
    processed:= true;
   end;
 end;
} 
 if (asender = wg_playlist) then
 with twidgetgrid(asender) do 
 begin
  if cellatpos(apos,co1) = ck_data then 
   begin
    s := '';
    for r := 0 to wg_playlist.rowhigh do   
      if wg_playlist.datacols[0].selected[i] then s := inttostr(r) + ',' + s;
    tstringdragobject.create(asender,adragobject,apos).data := s;
    processed:= true;
   end;
 end;


 if (asender.classname = 'twidgetgrid') then
 with twidgetgrid(asender) do begin
  if cellatpos(apos,co1) = ck_data then begin
   tstringdragobject.create(asender,adragobject,apos).data := inttostr(co1.row);
   processed:= true;
  end;
 end;
 
 if asender.classname = 'tfilelistview' then
 with tfilelistview(asender) do begin
  if cellatpos(apos,co1) = ck_data then begin
   tstringdragobject.create(asender,adragobject,apos).data := inttostr(co1.row);
   processed:= true;
  end;
 end;

end;

procedure tmainfo.on_beforedragdrop_pl(const asender: TObject; const apos: pointty;
                   var adragobject: tdragobject; var processed: Boolean);
var
 co1: gridcoordty;
 s : string;
 r, r2, r_old : integer;
begin
 if adragobject is tstringdragobject then begin
 {
  if (adragobject.sender = sg_playlist) then 
  with tstringgrid(asender) do begin
   if (asender = adragobject.sender) then begin
    if (cellatpos(apos,co1) = ck_data) then begin
    s := tstringdragobject(adragobject).data; 
    r := 0; r2 := 0;
    while system.pos(',',s) > 0 do 
      begin
        r_old := strtoint(copy(s,1,system.pos(',',s) - 1));
        moverow(r_old + r2, co1.row - r);
        player.playlist.move(r_old + r2, co1.row - r);
        delete(s,1,system.pos(',',s));
        if r_old <= co1.row then inc(r) else inc(r2);
      end;
      
    end;
    end;
   end; //with 
   }
  if (adragobject.sender = wg_playlist) then 
  with twidgetgrid(asender) do begin
   if (asender = adragobject.sender) then begin
    if (cellatpos(apos,co1) = ck_data) then begin
    s := tstringdragobject(adragobject).data; 
    r := 0; r2 := 0;
    while system.pos(',',s) > 0 do 
      begin
        r_old := strtoint(copy(s,1,system.pos(',',s) - 1));
        moverow(r_old + r2, co1.row - r);
        player.playlist.move(r_old + r2, co1.row - r);
        delete(s,1,system.pos(',',s));
        if r_old <= co1.row then inc(r) else inc(r2);
      end;
     findcurrentruck(false);
    end;
    end;
   end; //with 
      
   processed:= true;
   if (adragobject.sender = flv)or(adragobject.sender = wg_fl) then on_addfilefromdisk(b_add_from_flv);
   if (adragobject.sender = wg_myplaylists) then on_add_myplaylist_To_Playlist(b_add_myplaylist_To_Playlist);
   if (adragobject.sender = wg_last) then on_add_from_lastplayed(b_add_from_lastplayed);
   if (adragobject.sender = wg_tv)or(adragobject.sender = wg_radio) then 
   begin
     if tw_main.activepage = tp_radio then on_add_radioTVchanel_to_playlist(b_add_from_radiolist);
     if tw_main.activepage = tp_tv then on_add_radioTVchanel_to_playlist(b_add_from_tvlist);
   end;  
 end;
end;

procedure tmainfo.on_beforedragover_pl(const asender: TObject; const apos: pointty;
                   var adragobject: tdragobject; var accept: Boolean;
                   var processed: Boolean);
begin
 if adragobject is tstringdragobject then
   begin
    accept:= true;
    processed:= true;
   end;
end;

procedure tmainfo.on_dragbegin(const asender: TObject; const apos: pointty;
               var adragobject: tdragobject; var processed: Boolean);
begin
 //s_top.frame.caption := 'begin';s_top.update;
 processed:= true;
end;

procedure tmainfo.on_dragdrop(const asender: TObject; const apos: pointty;
               var adragobject: tdragobject; var processed: Boolean);
var
 ar1: msestringarty;
 po1: pdropfilesty;
 pc1,pc2: pchar;
 pw1,pw2: pmsechar;
 pend: pointer;
 i : integer;
 fs : tsearchrec;
 s : string;
 smse : msestring;
 
begin
 //s_top.frame.caption := 'dragdrop';s_top.update;
 if adragobject is tmimedragobject then begin
 
  with tmimedragobject(adragobject) do begin
   case wantedformatindex of 
    0: begin
     ar1:= breaklines(msestring(trim(data)));
    end;
    1: begin
     if length(data) > sizeof(dropfilesty) then begin
      po1:= pointer(data);
      pend:= pointer(po1)+length(data);
      if po1^.fwide then begin
       pw1:= pointer(po1) + po1^.pfiles;
       while pw1 < pend do begin
        pw2:= pw1;
        while (pw1 < pend) and (pw1^ <> #0) do begin
         inc(pw1);
        end;
        additem(ar1,psubstr(pw2,pw1));
        if pw1^ <> #0 then begin
         break;
        end;
        inc(pw1);
        if pw1^ = #0 then begin
         break;
        end;
       end;
      end
      else begin
       pc1:= pointer(po1) + po1^.pfiles;
       while pc1 < pend do begin
        pc2:= pc1;
        while (pc1 < pend) and (pc1^ <> #0) do begin
         inc(pc1);
        end;
        additem(ar1,psubstr(pc2,pc1));
        if pc1^ <> #0 then begin
         break;
        end;
        inc(pc1);
        if pc1^ <> #0 then begin
         break;
        end;
       end;
      end;
     end;
    end;
   end;
   if high(ar1) > -1 then
   for i := 0 to high(ar1) do 
     begin
       s := urldecode(ar1[i]);
{       smse := ar1[i];
       writeln('s1=',s);
       writeln('s2=',msestring(s) );
       writeln('s3=',ucs2to866(s) );
       writeln('s4=',ansitoutf8(s) );
       writeln('s5=',filename(s) );
       writeln('s6=',tomsefilepath(s) );
       writeln('s8=',tosysfilepath(s) );
       writeln('s9=',httpencode(s) );
       writeln('10=',myencodeurl(s) );
       writeln('11=',urldecode(s) );
}
       if system.pos('file://',s) = 1 then delete(s,1,7);
       if findfirst(s, faanyfile, fs) = 0 then
       repeat
       if (fs.name = '')or(fs.name = '.')or(fs.name = '..') then continue;
       if fs.Attr and faDirectory <> 0 
         then adddir(s)
         else addfile(extractfilename(s), s, '');    
       until findnext(fs) <> 0;
       findclose(fs);
     end;
  end;
  processed:= true;
 end;
end;

procedure tmainfo.on_dragover(const asender: TObject; const apos: pointty;
               var adragobject: tdragobject; var accept: Boolean; var processed: Boolean);
begin
 //s_top.frame.caption := 'over'; s_top.update;

 if adragobject is tmimedragobject then begin
  with tmimedragobject(adragobject) do begin
   actions:= [dnda_copy];
   if checkformat(msestringarty(knownformats)) then begin
    try
      if (widgetatpos(apos) = wg_playlist)or(widgetatpos(apos).parentwidget.name = 'wg_playlist') then accept:= true;
      processed:= true;
    except end;
   end;
  end;
 end;
end;

procedure tmainfo.on_dblclick_audiocd(const sender: TObject;
               var info: celleventinfoty);
begin
 if iscellclick(info,[ccr_dblclick]) 
    then
      if sg_cdplaylist.row >= 0 then
         begin
           player.tracknum := sg_cdplaylist.row;
           //player.connecttoaudiocd;
           player.OpenAudioCD(player.tracknum);
         end;
end;

procedure tmainfo.on_getaudiocdplaylist(const Sender: TObject; msg: AnsiString);
var i : integer;
begin
   sg_cdplaylist.clear;
   sg_cdplaylist.rowcount := player.playlist.count;
   if player.playlist.count > 0 then 
   for i := 0 to player.playlist.count - 1 do
       sg_cdplaylist.datacols[1][i] := player.playlist[i];
end;




end.

