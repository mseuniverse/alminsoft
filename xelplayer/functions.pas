unit functions;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
 function substrpos(substr,str:string):integer;
 function keypos(k, s : string) : integer;
 {SecondsToFmtStr was taken from Cactus Jukebox project}
 Function SecondsToFmtStr(seconds: longint): string;//converts integer to mm:ss time format   
 function whereisfile(fname : string; wait : boolean): string;
 Function UTF8toLatin1(utfstring: ansistring): ansistring;
 function pos_(substr, s : string) : integer;
 
implementation
uses process, classes;

/////////////////////////////////////////////////////
// определение позиции подстроки в строке
/////////////////////////////////////////////////////
function substrpos(substr,str:string):integer;
var      i, y : integer;
    possubstr : integer;
            b : boolean;
begin 
   possubstr := 0;
   b := false;
   
   if length(str) >= length(substr) 
     then  
       for i := 1 to length(str)do
         begin
           if str[i] = substr[1] 
             then 
                if length(substr)>1 
                 then
                  begin
                    b := true;
                    for y := 2 to length(substr) do
                      begin
                        b := true;
                        if substr[y] <> str[i+y-1] then 
                          begin
                            b := false;
                            break;
                          end;
                      end;
                 
                    if b = true
                      then begin
			                 possubstr := i;
		  				     break;
                           end; 
                  end
                 else 
                  begin
                    possubstr := i;
                    break;
                  end;  
         end;
   result := possubstr;  
end;  

/////////////////////////////////////////////////////
// определение позиции символа в строке
/////////////////////////////////////////////////////
function keypos(k, s : string) : integer;
var i : integer;
begin
{
 result := 0;
 for i := 1 to length(s) do
  if s[i] = k then 
   begin 
     result := i;
     break;
   end;
 }
 result := pos(k,s);  
end;

function pos_(substr, s : string) : integer;
begin
 result := system.pos(substr,s);  
end;

Function SecondsToFmtStr(seconds: longint): string;
Var min, sec, hour: longint;
  sm, ss, sh: string;
Begin
if seconds > 0 then 
 begin
  hour:= seconds div (60*60);

// === if minutes and hours ===
  if hour = 0                  
    then min := seconds div 60 
    else min := (seconds - 60 * 60 * hour) div 60;

//  ===   if only minutes   ===
//  min := seconds div 60;     

  sec := seconds Mod 60;
  
  str(min, sm);
  str(sec, ss);
  str(hour, sh);
  
  if min<10  then sm := '0' + sm;
  if sec<10  then ss := '0' + ss;
  if hour<10 then sh := '0' + sh;
  
  result := sh + ':' + sm + ':' + ss;
 end 
 else Result:='00:00:00';
End;
        
//========================================
//      ищем файл через whereis
//========================================
function whereisfile(fname : string; wait : boolean): string;
var p : tprocess;
   sl:tstringlist;
   s : string;
begin
p := tprocess.create(nil);
sl:= tstringlist.create;
if wait then p.options := p.options + [powaitonexit,pousepipes];
if not wait then p.options := p.options + [pousepipes];
p.commandline := 'whereis '+fname;
p.execute;
sl.loadfromstream(p.output);
s := sl[0];
sl.free;
p.free;
delete(s,1,pos(':',s));
if s = '' then
   begin
     result := s;
     exit;
   end;
delete(s,1,1);
delete(s,pos(fname,s),length(s) - pos(fname,s) + 1);
result := s;
end;

Function UTF8toLatin1(utfstring: ansistring): ansistring;

Var i: integer;
  tmps: string;
  utf16: boolean;
Begin
  i := 0;
  tmps := '';
  utf16 := false;

  If length(utfstring)>0 Then
    Begin
      Repeat
        Begin
          inc(i);
          Case byte(utfstring[i]) Of 
            $ff: If byte(utfstring[i+1])=$fe Then utf16 := true;
            $c3:
                 Begin
                   Delete(utfstring, i, 1);
                   utfstring[i] := char(byte(utfstring[i])+64);
                 End;
            $c2:
                 Begin
                   Delete(utfstring, i, 1);
                   dec(i);
                 End;
          End;
        End;
      Until (i>=length(utfstring)-1) Or utf16;
      //if utf16 detected
      If utf16 Then
        Begin
          i := i+2;
          writeln('utf16');
          Repeat
            Begin
              inc(i);
              If byte(utfstring[i])<>0 Then tmps := tmps+utfstring[i];
            End;
          Until (i>=length(utfstring));
        End;
    End;

  If Not utf16 Then result := utfstring
  Else Result := tmps;
End;

end.
