unit set2form;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,mseforms,msesplitter,
 msesimplewidgets,msewidgets,msetimer,msescrollbar,msestatfile,msestream,
 msestrings,msetabs,sysutils,msegraphedits,mseifiglob,msetypes;
 
type
 tset2fo = class(tmseform)
   timer_move: ttimer;
   trichbutton1: trichbutton;
   procedure on_move(const sender: TObject);
   procedure on_childmouseevens(const sender: twidget;
                   var ainfo: mouseeventinfoty);
   procedure on_close(const sender: TObject);
 end;
var
 set2fo: tset2fo;
 moveXX, moveYY : integer; 
 
implementation
uses
 set2form_mfm, main;
 
procedure tset2fo.on_move(const sender: TObject);
begin
  left := gui_getpointerpos.x - moveXX;
  top := gui_getpointerpos.y - moveYY;
end;

procedure tset2fo.on_childmouseevens(const sender: twidget;
               var ainfo: mouseeventinfoty);
var s : string;        
    r : real;       
begin
  if (ainfo.eventkind = ek_buttonpress) then 
  BEGIN
   bringtofront;
   if (sender.classname = 'tslider') then 
   with mainfo do
   begin
     s := (sender as tslider).name;
     delete(s,1,2);
     s := 'pb' + s;
     r := (gui_getpointerpos.x - set2fo.left - (sender as tslider).left) / (sender as tslider).width;
     (sender as tslider).value := r;
   end;
   if {(s_top = sender)or}{(sender.classname = 'ttabpage')}
      {or}(sender.classname = 'tcustomtabbar1')
      or(sender.classname = 'tscrollbox') then 
    begin
       moveXX := gui_getpointerpos.x - left;
       moveYY := gui_getpointerpos.y - top;
       timer_move.enabled := true; 
    end;
  END;
  if (ainfo.eventkind = ek_buttonrelease) then 
   begin
   if timer_move.enabled then timer_move.enabled := false;
   end; 
end;

procedure tset2fo.on_close(const sender: TObject);
begin
  close
end;

end.
