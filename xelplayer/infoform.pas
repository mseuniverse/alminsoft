unit infoform;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,mseforms,msesplitter,
 msesimplewidgets,msewidgets,msetimer,msedock,msewindowwidget,mseimage,
 msedataedits,mseedit,mseifiglob,msestrings,msetypes, mp3file;
type
 tinfofo = class(tmseform)
   s_top: tspacer;
   trichbutton1: trichbutton;
   timer_move: ttimer;
   timer_resize: ttimer;
   tdockhandle1: tdockhandle;
   tscrollbox2: tscrollbox;
   l_trackinfo: tlabel;
   tlabel3: tlabel;
   im_full: timage;
   tspacer1: tspacer;
   trichbutton4: trichbutton;
   se_: tstringedit;
   trichbutton2: trichbutton;
   procedure on_resize(const sender: TObject);
   procedure on_move(const sender: TObject);
   procedure on_childmouseevens(const sender: twidget;
                   var ainfo: mouseeventinfoty);
   procedure on_close(const sender: TObject);
   procedure on_preview(const sender: TObject);
   procedure on_showtags(const sender: TObject);
 end;
var
 infofo: tinfofo;
 moveXX, moveYY : integer;
 mp3f : TMP3File;
 
implementation
uses
 infoform_mfm, main, fileinfo, sysutils;


procedure tinfofo.on_resize(const sender: TObject);
begin
  width := gui_getpointerpos.x - moveXX;
  height := gui_getpointerpos.y - moveYY;
end;

procedure tinfofo.on_move(const sender: TObject);
begin
  left := gui_getpointerpos.x - moveXX;
  top := gui_getpointerpos.y - moveYY;
end;

procedure tinfofo.on_childmouseevens(const sender: twidget;
               var ainfo: mouseeventinfoty);
begin
  if (ainfo.eventkind = ek_buttonpress) then 
  BEGIN
   bringtofront;
   if (s_top = sender)or(se_ = sender)or(l_trackinfo = sender)or(im_full = sender)
   or(infofo = sender)or(sender.classname = 'ttabpage')
      or(sender.classname = 'tcustomtabbar1')
      {or(sender.classname = 'tscrollbox')} then 
    begin
       moveXX := gui_getpointerpos.x - left;
       moveYY := gui_getpointerpos.y - top;
       timer_move.enabled := true; 
    end;
   if (tdockhandle1 = sender) then 
    begin
       moveXX := gui_getpointerpos.x - width;
       moveYY := gui_getpointerpos.y - height;
       timer_resize.enabled := true;
    end;
  END;
  if (ainfo.eventkind = ek_buttonrelease) then 
   begin
   if timer_move.enabled then timer_move.enabled := false;
   if timer_resize.enabled then timer_resize.enabled := false;
   end;   
end;

procedure tinfofo.on_close(const sender: TObject);
begin
  close;
end;

procedure tinfofo.on_preview(const sender: TObject);
begin
//  mainfo.getpreview(l_.caption);
  mainfo.getpreview(se_.value{l_.caption});
end;

procedure tinfofo.on_showtags(const sender: TObject);
var i, r : integer;
    s : string;
begin
  r := mainfo.wg_playlist.row;
  if r < 0 then exit;
  s := mainfo.se_url[r];
  {$ifdef windows}
  for i := 1 to length(s) do if s[i] = '/' then s[i] := '\';
  if s[1] = '/' then delete(s,1,1);
  {$endif}
  mp3f := TMP3File.create;
  mp3f.ReadTag(s);
  fileinfofo.SE_FILE.value := s;
  fileinfofo.se_artist.value := mp3f.Artist;
  fileinfofo.se_Album.value := mp3f.Album;
  fileinfofo.se_Title.value := mp3f.Title;
  fileinfofo.se_Track.value := mp3f.Track;
  fileinfofo.se_Year.value := mp3f.Year;
  fileinfofo.se_Comment.value := mp3f.Comment;
  fileinfofo.se_Genre.value := mp3f.Genre;
  fileinfofo.se_Playtime.value := mp3f.Playtime;
  fileinfofo.se_Playlength.value := inttostr(mp3f.Playlength);
  fileinfofo.se_Bitrate.value := inttostr(mp3f.Bitrate);
  fileinfofo.se_Samplerate.value := inttostr(mp3f.Samplerate);
  mp3f.free;
  fileinfofo.show(true);
end;

end.
